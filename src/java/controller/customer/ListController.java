/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import context.CustomerDAO;
import context.RoleDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import model.Role;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
public class ListController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
//        processRequest(req, resp);
        CustomerDAO customerDB = new CustomerDAO();
        //       ArrayList<User> user = userDB.list();
//        RoleDAO roleDB = new RoleDAO();
//        ArrayList<Role> role = roleDB.list();

        int pagesize = Integer.parseInt(getServletContext().getInitParameter("pagesize"));
        String page = req.getParameter("page");
        if (page == null || page.length() == 0) {
            page = "1";
        }
        int pageindex = Integer.parseInt(page);
        ArrayList<User> customer = customerDB.pagging(pageindex, pagesize);
        int totalRow = customerDB.count();
        int totalpage = (totalRow % pagesize == 0) ? totalRow / pagesize
                : (totalRow / pagesize) + 1;

        req.setAttribute("pageindex", pageindex);
        req.setAttribute("totalpage", totalpage);
//        req.setAttribute("role", role);
        req.setAttribute("customer", customer);
        req.getRequestDispatcher("/customer/list.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
//        processRequest(req, resp);
        String searched = req.getParameter("searched");
        String status = req.getParameter("status");

        CustomerDAO customerDB = new CustomerDAO();
        if (searched != null) {
            ArrayList<User> customer = customerDB.searchByText(searched);
            req.setAttribute("customer", customer);
        }
        if (!status.equals("- Status -")) {
            ArrayList<User> customer = customerDB.searchByStatus(status);
            req.setAttribute("customer", customer);
        }
        if (searched != null && !status.equals("- Status -")) {
            ArrayList<User> customer = customerDB.searchUser1(searched, status);
            req.setAttribute("customer", customer);
        }
        req.getRequestDispatcher("/customer/list.jsp").forward(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}