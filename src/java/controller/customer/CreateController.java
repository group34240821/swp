/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import context.CustomerDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 5, // 5MB
        maxRequestSize = 1024 * 1024 * 50)
public class CreateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.getRequestDispatcher("/customer/create.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String password = req.getParameter("password");
        String fullname = req.getParameter("fullname");
        String gender = req.getParameter("gender");
        String email = req.getParameter("email");
        String mobile = req.getParameter("mobile");
        String address = req.getParameter("address");
        String status = req.getParameter("status");

        CustomerDAO customerDB = new CustomerDAO();
        User customer = customerDB.getEmail(email);
        if (customer != null) {
            req.setAttribute("error_email", "Your Email is being used. Please try again!");
            req.getRequestDispatcher("/customer/create.jsp").forward(req, resp);
        } else {
            Part file = req.getPart("img");
            String fileName = extractFileName(file);  // get selected image file name
            if (fileName.equals("")) {
                User u = new User();
                u.setPassword(password);
                u.setFullname(fullname);
                u.setGender(gender);
                u.setEmail(email);
                u.setMobile(mobile);
                u.setAddress(address);
                u.setStatus(status);

                customerDB.create(u);
                customerDB.insert_changes_history(u);
            } else {
                String applicationPath = getServletContext().getRealPath(""); //build folder url
                String uploadPath = applicationPath + "assets\\img\\user"; //the path to the folder where the image is saved
                File fileUploadDirectory = new File(uploadPath);
                //If the path does not exist then create path
                if (!fileUploadDirectory.exists()) {
                    fileUploadDirectory.mkdirs();
                }
                String savePath = uploadPath + File.separator + fileName;
                file.write(savePath + File.separator);

                User u = new User();
                u.setPassword(password);
                u.setFullname(fullname);
                u.setGender(gender);
                u.setEmail(email);
                u.setMobile(mobile);
                u.setAddress(address);
                u.setStatus(status);
                u.setImg(fileName);

                customerDB.create(u);
                customerDB.insert_changes_history(u);
            }
            resp.sendRedirect("customer-list");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String extractFileName(Part file) {
        String contentDisp = file.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

}
