/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import context.CustomerDAO;
import context.RoleDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import model.Role;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
public class EditController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int userid = Integer.parseInt(req.getParameter("userid"));
        CustomerDAO customerDB = new CustomerDAO();
        User customer = customerDB.get(userid);
        ArrayList<User> cus = customerDB.list_changes_history(userid);
                
//        RoleDAO roleDB = new RoleDAO();
//        ArrayList<Role> role = roleDB.list();

        req.setAttribute("customer", customer);
        req.setAttribute("cus", cus);
//        req.setAttribute("role", role);
        req.getRequestDispatcher("/customer/edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int userid = Integer.parseInt(req.getParameter("userid"));
        String fullname = req.getParameter("fullname");
        String gender = req.getParameter("gender");
        String email = req.getParameter("email");
        String mobile = req.getParameter("mobile");
        String address = req.getParameter("address");
        String status = req.getParameter("status");

        User u = new User();
        u.setUserid(userid);
        u.setFullname(fullname);
        u.setGender(gender);
        u.setEmail(email);
        u.setMobile(mobile);
        u.setAddress(address);
        u.setStatus(status);

        CustomerDAO customerDB = new CustomerDAO();
        customerDB.edit(u);
        customerDB.insert_changes_history(u);
        resp.sendRedirect("customer-list");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
