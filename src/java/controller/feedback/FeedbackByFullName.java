/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.feedback;

import context.FeedbackDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Feedback;

/**
 *
 * @author Pc
 */
@WebServlet(name = "FeedbackByFullName", urlPatterns = {"/searchfeedbackfullname"})
public class FeedbackByFullName extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            int pagesize = 5;
            response.setContentType("text/html;charset=UTF-8");
            FeedbackDAO dao = new FeedbackDAO();
            String index = request.getParameter("paging");
            if (index == null) {
                index = "1";
            }
            String username = request.getParameter("searchFullName");
            if (username == null) {
                dao.getAll(pagesize, Integer.valueOf(index));
            } else {
                dao.searchByFullName(username, pagesize, Integer.valueOf(index));
            }
            List<Feedback> list = dao.getList();
            int pagetotals = dao.count() / pagesize;
            if ((dao.count() % pagesize) > 0) {
                pagetotals += 1;
            }
            request.setAttribute("PageTotal", pagetotals);
            request.setAttribute("List_Feedback", list);
            request.getRequestDispatcher("Feedback.jsp").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DefaultFeedBack.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
