/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.product2;

import context.ProductDAO2;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Product2;
import model.Category2;

/**
 *
 * @author Cao Gia Linh
 */
public class DetailsProduct extends HttpServlet {

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProductDAO2 product = new ProductDAO2();
//        String indexPage = req.getParameter("index");
        int product_id = Integer.parseInt(req.getParameter("product_id"));

//        if (indexPage == null) {
//            indexPage = "1";
//        }
//        int index = Integer.parseInt(indexPage);
//        int count = product.count();
//        int endPage = count / 5;
//        if (count % 3 != 0) {
//            //endPage = 1;
//            endPage++;
//        }
        
//        List<Product2> listP = product.pagging(index);
        List<Product2> listPd = product.getByID(product_id);
        
        List<Category2> listC = product.GetCategoryList();
        List<Product2> listPL = product.GetProductList();
        req.setAttribute("listPL", listPL);
        req.setAttribute("listC", listC);

//        req.setAttribute("listP", listP);
        req.setAttribute("listPd", listPd);
        req.setAttribute("listC", listC);
//        req.setAttribute("endPage", endPage);

        req.getRequestDispatcher("product/ProductDetail.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

}
