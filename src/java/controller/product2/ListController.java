/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.product2;

import context.ProductDAO2;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
//import java.util.ArrayList;
import java.util.List;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import model.Product2;
import model.Category2;

/**
 *
 * @author Cao Gia Linh
 */
public class ListController extends HttpServlet {

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProductDAO2 product = new ProductDAO2();

        String indexPage = req.getParameter("index");
        if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        int count = product.count();
        int endPage = count / 5;
        if (count % 3 != 0) {
            //endPage = 1;
            endPage++;
        }

        List<Product2> listPL = product.GetProductList();
        List<Product2> listP = product.pagging(index);
        List<Category2> listC = product.GetCategoryList();
        req.setAttribute("listP", listP);
        req.setAttribute("listPL", listPL);
        req.setAttribute("listC", listC);
        req.setAttribute("endPage", endPage);

        req.getRequestDispatcher("product/ProductList.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

}
