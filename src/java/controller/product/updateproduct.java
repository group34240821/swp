/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.product;

import context.ProductDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import model.Brand;
import model.category1;
import model.product1;

/**
 *
 * @author BUI QUOC BAO
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 5, // 5MB
        maxRequestSize = 1024 * 1024 * 50)
public class updateproduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet updateproduct</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet updateproduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO dao = new ProductDAO();
        int product_id = Integer.parseInt(request.getParameter("product_id"));
        ArrayList<product1> p = dao.getByID(product_id);
        ArrayList<category1> c = dao.getallC();
        ArrayList<Brand> b = dao.getallB();
        request.setAttribute("c", c);
        request.setAttribute("b", b );
        request.setAttribute("p", p);
        request.getRequestDispatcher("designer/update_product.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int product_id = Integer.parseInt(request.getParameter("product_id"));
        String product_name = request.getParameter("product_name");
        Double price = Double.parseDouble(request.getParameter("price"));
        Double sale = Double.parseDouble(request.getParameter("sale"));
        String description = request.getParameter("description");
        
        
        Part file = request.getPart("img");
        String fileName = extractFileName(file);  // get selected image file name
        String applicationPath = getServletContext().getRealPath("");
        String uploadPath = applicationPath + "assets\\img\\user";
        File fileUploadDirectory = new File(uploadPath);
        if (!fileUploadDirectory.exists()) {
            fileUploadDirectory.mkdirs();
        }
        String savePath = uploadPath + File.separator + fileName;
//        String sRootPath = new File(savePath).getAbsolutePath();
        file.write(savePath + File.separator);
//        File fileSaveDir1 = new File(savePath);
//        file.write(savePath + File.separator);

        int brand_id = Integer.parseInt(request.getParameter("brand_id"));
        int category_id = Integer.parseInt(request.getParameter("category_id"));
        String status = request.getParameter("status");
        ProductDAO dao = new ProductDAO();
        dao.update(product_id, product_name, price, sale, description, fileName, brand_id, category_id, status);
        response.sendRedirect("listproduct");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private String extractFileName(Part file) {
        String contentDisp = file.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

}
