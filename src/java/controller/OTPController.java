/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import context.DBContext;
import context.UserDAO2;
import helper.EmailConfig;
import helper.helper;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import javax.mail.MessagingException;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
/**
 *
 * @author toden
 */
public class OTPController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OTPController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OTPController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        new DBContext();
        try {
            //processRequest(request, response);
            HttpSession session = request.getSession();
            String OTP = helper.generateRandomString();
            String email = request.getParameter("email");
            String resend = request.getParameter("resend") == null ? "n" : request.getParameter("resend");
            if (resend != "n") {
                new EmailConfig().SendEmail((String) session.getAttribute("reEmail"), "Your OTP code: " + OTP);
                session.setAttribute("OTP", OTP);
                request.getRequestDispatcher("OTPPage.jsp").forward(request, response);

            } else if (UserDAO2.checkAuthByEmail(email) == null) {
                request.setAttribute("err", "email doesnt exist");
                request.getRequestDispatcher("reset-password.jsp").forward(request, response);
            } else {
                new EmailConfig().SendEmail(email, "Your OTP code: " + OTP);
                session.setAttribute("reEmail", email);
                session.setAttribute("OTP", OTP);
                request.getRequestDispatcher("OTPPage.jsp").forward(request, response);
            }
        } catch (SQLException ex) {
            request.getRequestDispatcher("404.html").forward(request, response);
        } catch (MessagingException ex) {
            request.getRequestDispatcher("404.html").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        HttpSession session = request.getSession();
        String otp = (String) session.getAttribute("OTP");
        String reEmail = (String) session.getAttribute("reEmail");
        String[] otpParts = request.getParameterValues("otp");
        String inputotp = "";

        for (String i : otpParts) {
            inputotp += i;
        }

        if (inputotp.trim().equals(otp)) {
            try {
                String resetPass = helper.generateRandomString();
                EmailConfig ec = new EmailConfig();
                ec.SendEmail(reEmail, "Your reset password is " + resetPass);
                UserDAO2.UpdatePassUser(resetPass, UserDAO2.checkAuthByEmail(reEmail).getId());
                session.setAttribute("otp", null);
                session.setAttribute("forgotPassEmail", null);
                request.getRequestDispatcher("index.html").forward(request, response);
            } catch (Exception ex) {
                request.getRequestDispatcher("404.html").forward(request, response);
            }

        } else {
            request.setAttribute("err", "incorrect otp or out of date");
            request.getRequestDispatcher("OTPPage.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
