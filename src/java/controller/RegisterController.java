/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import context.DBContext;
import context.UserDAO2;
import helper.EmailConfig;
import helper.helper;
//import verify.SendEmail;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import javax.mail.MessagingException;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
import model.Role;
import model.User;
import model.User1;


/**
 *
 * @author Admin
 */
public class RegisterController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("Register.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //get info from form
        String fullname = request.getParameter("fname");
        String username = request.getParameter("uname");
        String gender = request.getParameter("gender");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String email = request.getParameter("email");
        String password = request.getParameter("pass");
        String re_pass = request.getParameter("re_pass");
        int roleid = Integer.parseInt("4");
        String status = "Active";

        Role r = new Role();
        r.setRoleid(roleid);

        User u = new User();
        u.setUsername(username);
        u.setPassword(password);
        u.setFullname(fullname);
        u.setGender(gender);
        u.setEmail(email);
        u.setMobile(phone);
        u.setAddress(address);
        u.setRoles(r);
        u.setStatus(status);

        UserDAO2 dao = new UserDAO2();

        User1 ur = dao.getUser(email);
        if (ur != null) {
            request.setAttribute("error", "The account is already exist");
            request.getRequestDispatcher("Register.jsp").forward(request, response);

        } else if (!password.equals(re_pass)) {
            request.setAttribute("error", "Password and repass must same");
            request.getRequestDispatcher("Register.jsp").forward(request, response);
        } else {
            new DBContext();
            try {
                //processRequest(request, response);
                HttpSession session = request.getSession();
                String OTP = helper.generateRandomString();
                String resend = request.getParameter("resend") == null ? "n" : request.getParameter("resend");
                if (resend != "n") {
                    new EmailConfig().SendEmail((String) session.getAttribute("reEmail"), "Your OTP code: " + OTP);
                    session.setAttribute("OTP", OTP);
                    request.getRequestDispatcher("OTPPageRegister.jsp").forward(request, response);
                } else {
                    new EmailConfig().SendEmail(email, "Your OTP code: " + OTP);
                    session.setAttribute("reEmail", email);
                    session.setAttribute("OTP", OTP);
                    session.setAttribute("us", u);
                    request.getRequestDispatcher("OTPPageRegister.jsp").forward(request, response);
                }
            } catch (MessagingException ex) {
                request.getRequestDispatcher("404.html").forward(request, response);
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
