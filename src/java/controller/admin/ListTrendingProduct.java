/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import context.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Product;
/**
 *
 * @author DW
 */
@WebServlet(name = "ListTrendingProduct", urlPatterns = {"/trending-product"})
public class ListTrendingProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListTrendingProduct</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListTrendingProduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO productDAO = new ProductDAO();

        try {
            String startDate = request.getParameter("startDate") == null ? "" : request.getParameter("startDate");
            String endDate = request.getParameter("endDate") == null ? "" : request.getParameter("endDate");
            int pagesize = Integer.parseInt(getServletContext().getInitParameter("pagesize"));
            String page = request.getParameter("page");
            if (page == null || page.length() == 0) {
                page = "1";
            }
            int pageindex = Integer.parseInt(page);
            ArrayList<Product> product = productDAO.paggingProduct(startDate, endDate, pageindex, pagesize);
            request.setAttribute("products", product);
            int totalRow = product.size();
            int totalpage = (totalRow % pagesize == 0) ? totalRow / pagesize
                    : (totalRow / pagesize) + 1;

            request.setAttribute("pageindex", pageindex);
            request.setAttribute("totalpage", totalpage);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher("designer/list-trending-products.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO productDAO = new ProductDAO();

        try {
            String startDate = request.getParameter("startDate") == null ? "" : request.getParameter("startDate");
            String endDate = request.getParameter("endDate") == null ? "" : request.getParameter("endDate");
            int pagesize = Integer.parseInt(getServletContext().getInitParameter("pagesize"));
            String page = request.getParameter("page");
            if (page == null || page.length() == 0) {
                page = "1";
            }
            int pageindex = Integer.parseInt(page);
            ArrayList<Product> product = productDAO.paggingProduct(startDate, endDate, pageindex, pagesize);
            request.setAttribute("products", product);
            int totalRow = product.size();
            int totalpage = (totalRow % pagesize == 0) ? totalRow / pagesize
                    : (totalRow / pagesize) + 1;

            request.setAttribute("pageindex", pageindex);
            request.setAttribute("totalpage", totalpage);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher("designer/list-trending-products.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
