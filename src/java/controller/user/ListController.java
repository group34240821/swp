/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user;

import context.RoleDAO;
import context.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import model.Role;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
//@WebServlet(name = "ListController", urlPatterns = {"/ListController"})
public class ListController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
//        processRequest(req, resp);
        UserDAO userDB = new UserDAO();
        //       ArrayList<User> user = userDB.list();
        RoleDAO roleDB = new RoleDAO();
        ArrayList<Role> role = roleDB.list();

        int pagesize = Integer.parseInt(getServletContext().getInitParameter("pagesize"));
        String page = req.getParameter("page");
        if (page == null || page.length() == 0) {
            page = "1";
        }
        int pageindex = Integer.parseInt(page);
        ArrayList<User> user = userDB.pagging(pageindex, pagesize);
        int totalRow = userDB.count();
        int totalpage = (totalRow % pagesize == 0) ? totalRow / pagesize
                : (totalRow / pagesize) + 1;

        req.setAttribute("pageindex", pageindex);
        req.setAttribute("totalpage", totalpage);
        req.setAttribute("role", role);
        req.setAttribute("user", user);
        req.getRequestDispatcher("/user/list.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
//        processRequest(req, resp);
        String searched = req.getParameter("searched");
        String gender = req.getParameter("gender");
        String rolename = req.getParameter("rolename");
        String status = req.getParameter("status");

        UserDAO userDB = new UserDAO();
        if (searched != null) {
            ArrayList<User> user = userDB.searchByText(searched);
            req.setAttribute("user", user);
        }
        if (!gender.equals("- Gender -")) {
            ArrayList<User> user = userDB.searchByGender(gender);
            req.setAttribute("user", user);
        }
        if (!rolename.equals("- Role -")) {
            ArrayList<User> user = userDB.searchByRolename(rolename);
            req.setAttribute("user", user);
        }
        if (!status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchByStatus(status);
            req.setAttribute("user", user);
        }
        if (searched != null && !gender.equals("- Gender -")
                && !rolename.equals("- Role -")
                && !status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser1(searched, gender, rolename, status);
            req.setAttribute("user", user);
        }
        if (searched != null && !gender.equals("- Gender -")
                && rolename.equals("- Role -")
                && status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser2(searched, gender);
            req.setAttribute("user", user);
        }
        if (searched != null && gender.equals("- Gender -")
                && !rolename.equals("- Role -")
                && status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser3(searched, rolename);
            req.setAttribute("user", user);
        }
        if (searched != null && gender.equals("- Gender -")
                && rolename.equals("- Role -")
                && !status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser4(searched, status);
            req.setAttribute("user", user);
        }
        if (searched != null && !gender.equals("- Gender -")
                && !rolename.equals("- Role -")
                && status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser5(searched, gender, rolename);
            req.setAttribute("user", user);
        }
        if (searched != null && !gender.equals("- Gender -")
                && rolename.equals("- Role -")
                && !status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser6(searched, gender, status);
            req.setAttribute("user", user);
        }
        if (searched != null && gender.equals("- Gender -")
                && !rolename.equals("- Role -")
                && !status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser7(searched, rolename, status);
            req.setAttribute("user", user);
        }
        if (searched == null && gender.equals("- Gender -")
                && !rolename.equals("- Role -")
                && status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser8(gender, rolename);
            req.setAttribute("user", user);
        }
        if (searched == null && !gender.equals("- Gender -")
                && rolename.equals("- Role -")
                && !status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser9(gender, status);
            req.setAttribute("user", user);
        }
        if (searched == null && !gender.equals("- Gender -")
                && !rolename.equals("- Role -")
                && !status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser10(gender, rolename, status);
            req.setAttribute("user", user);
        }
        if (searched == null && gender.equals("- Gender -")
                && !rolename.equals("- Role -")
                && !status.equals("- Status -")) {
            ArrayList<User> user = userDB.searchUser11(rolename, status);
            req.setAttribute("user", user);
        }

        req.getRequestDispatcher("/user/list.jsp").forward(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
