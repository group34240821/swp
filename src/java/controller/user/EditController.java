/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user;

import context.RoleDAO;
import context.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import model.Role;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 5, // 5MB
        maxRequestSize = 1024 * 1024 * 50)
public class EditController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int userid = Integer.parseInt(req.getParameter("userid"));
        UserDAO userDB = new UserDAO();
        User user = userDB.get(userid);

        RoleDAO roleDB = new RoleDAO();
        ArrayList<Role> role = roleDB.list();

        req.setAttribute("user", user);
        req.setAttribute("role", role);
        req.getRequestDispatcher("/user/edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String userid = req.getParameter("userid");
//        String username = req.getParameter("username");
//        String password = req.getParameter("password");
        String fullname = req.getParameter("fullname");
        String gender = req.getParameter("gender");
        String email = req.getParameter("email");
        String mobile = req.getParameter("mobile");
        String address = req.getParameter("address");
        String roleid = req.getParameter("roleid");
        String status = req.getParameter("status");

//        Part file = req.getPart("img");
//        String fileName = extractFileName(file);  // get selected image file name
//        String applicationPath = getServletContext().getRealPath("");
//        String uploadPath = applicationPath + "assets\\img\\user";
//        File fileUploadDirectory = new File(uploadPath);
//        if (!fileUploadDirectory.exists()) {
//            fileUploadDirectory.mkdirs();
//        }
//        String savePath = uploadPath + File.separator + fileName;
//        file.write(savePath + File.separator);

        Role r = new Role();
        r.setRoleid(Integer.parseInt(roleid));

        User u = new User();
        u.setUserid(Integer.parseInt(userid));
//        u.setUsername(username);
//        u.setPassword(password);
        u.setFullname(fullname);
        u.setGender(gender);
        u.setEmail(email);
        u.setMobile(mobile);
        u.setAddress(address);
        u.setRoles(r);
        u.setStatus(status);
//        u.setImg(fileName);

        UserDAO userDB = new UserDAO();
        userDB.edit(u);
        resp.sendRedirect("user-list");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

//    private String extractFileName(Part file) {
//        String contentDisp = file.getHeader("content-disposition");
//        String[] items = contentDisp.split(";");
//        for (String s : items) {
//            if (s.trim().startsWith("filename")) {
//                return s.substring(s.indexOf("=") + 2, s.length() - 1);
//            }
//        }
//        return "";
//    }

}
