/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.post;

import context.PostDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Post;

/**
 *
 * @author Mạc Huyền
 */
public class EditFlagController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int postid = Integer.parseInt(req.getParameter("postid"));
        String flag = req.getParameter("flag");
        
        PostDAO postDB = new PostDAO();
        Post p = new Post();
        p.setPostid(postid);
        p.setFlag(flag);

        postDB.editFlag(p);
        resp.sendRedirect("post-list");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
//        int postid = Integer.parseInt(req.getParameter("postid"));
//
//        CategoryDAO categoryDB = new CategoryDAO();
//        ArrayList<Category> category = categoryDB.list();
//
//        UserDAO authorDB = new UserDAO();
//        ArrayList<User> author = authorDB.listAuthor();
//
//        PostDAO postDB = new PostDAO();
//        int pagesize = Integer.parseInt(getServletContext().getInitParameter("pagesize"));
//        String page = req.getParameter("page");
//        if (page == null || page.length() == 0) {
//            page = "1";
//        }
//        int pageindex = Integer.parseInt(page);
//        ArrayList<Post> post = postDB.pagging(pageindex, pagesize);
//        int totalRow = postDB.count();
//        int totalpage = (totalRow % pagesize == 0) ? totalRow / pagesize
//                : (totalRow / pagesize) + 1;
//
//        postDB.get(postid);
//
//        req.setAttribute("pageindex", pageindex);
//        req.setAttribute("totalpage", totalpage);
//        req.setAttribute("category", category);
//        req.setAttribute("author", author);
//        req.setAttribute("post", post);
//        req.getRequestDispatcher("/post/list.jsp").forward(req, resp);
        processRequest(req, resp);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
//        int postid = Integer.parseInt(req.getParameter("postid"));
//        String flag = req.getParameter("flag");
//
//        Post p = new Post();
//        p.setPostid(postid);
//        p.setFlag(flag);
//
//        PostDAO postDB = new PostDAO();
//        postDB.editFlag(p);
//        resp.sendRedirect("post-list");
        processRequest(req, resp);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
