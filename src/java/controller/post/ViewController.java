/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.post;

import context.CategoryDAO;
import context.PostDAO;
import context.ProductDAO;
import context.UserDAO;
import java.io.IOException;
import java.util.ArrayList;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Category;
import model.Post;
import model.User;
import model.category1;

/**
 *
 * @author Mạc Huyền
 */
public class ViewController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int postid = Integer.parseInt(req.getParameter("postid"));
        PostDAO postDB = new PostDAO();
        Post post = postDB.get(postid);
        ArrayList<Post> postRecent = postDB.recentPost();
        
        CategoryDAO categoryDB = new CategoryDAO();
        ArrayList<Category> category = categoryDB.list();
        
        UserDAO authorDB = new UserDAO();
        ArrayList<User> author = authorDB.listAuthor();
        
        ProductDAO dao1 = new ProductDAO();
        ArrayList<category1> c = dao1.getallC();
        req.setAttribute("post", post);
        req.setAttribute("postRecent", postRecent);
        req.setAttribute("category", category);
        req.setAttribute("author", author);
        req.setAttribute("c", c);
        req.getRequestDispatcher("/post/post-detail.jsp").forward(req, resp);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
