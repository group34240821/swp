/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.post;

import context.CategoryDAO;
import context.PostDAO;
import context.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import model.Category;
import model.Post;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
//@WebServlet(name = "ListController", urlPatterns = {"/ListController"})
public class ListController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        CategoryDAO categoryDB = new CategoryDAO();
        ArrayList<Category> category = categoryDB.list();

        UserDAO authorDB = new UserDAO();
        ArrayList<User> author = authorDB.listAuthor();

        PostDAO postDB = new PostDAO();
        int pagesize = Integer.parseInt(getServletContext().getInitParameter("pagesize"));
        String page = req.getParameter("page");
        if (page == null || page.length() == 0) {
            page = "1";
        }
        int pageindex = Integer.parseInt(page);
        ArrayList<Post> post = postDB.pagging(pageindex, pagesize);
        int totalRow = postDB.count();
        int totalpage = (totalRow % pagesize == 0) ? totalRow / pagesize
                : (totalRow / pagesize) + 1;

        req.setAttribute("pageindex", pageindex);
        req.setAttribute("totalpage", totalpage);
        req.setAttribute("category", category);
        req.setAttribute("author", author);
        req.setAttribute("post", post);
        req.getRequestDispatcher("/post/list.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String searched = req.getParameter("searched");
        String category = req.getParameter("category");
        String author = req.getParameter("author");
        String status = req.getParameter("status");
        
        CategoryDAO categoryDB = new CategoryDAO();
        ArrayList<Category> cate = categoryDB.list();
        UserDAO authorDB = new UserDAO();
        ArrayList<User> auth = authorDB.listAuthor();
        req.setAttribute("category", cate);
        req.setAttribute("author", auth);

        PostDAO postDB = new PostDAO();
        if (searched != null) {
            ArrayList<Post> post = postDB.searchByText(searched);
            req.setAttribute("post", post);
        }
        if (category != null) {
            ArrayList<Post> post = postDB.searchByCategory(category);
            req.setAttribute("post", post);
        }
        if (author != null) {
            ArrayList<Post> post = postDB.searchByAuthor(author);
            req.setAttribute("post", post);
        }
        if (status != null) {
            ArrayList<Post> post = postDB.searchByStatus(status);
            req.setAttribute("post", post);
        }
        if (searched != null && category != null && author != null && status != null) {
            ArrayList<Post> post = postDB.searchPost11(searched, category, author, status);
            req.setAttribute("post", post);
        }
        if (searched != null && category != null && author == null && status == null) {
            ArrayList<Post> post = postDB.searchPost1(searched, category);
            req.setAttribute("post", post);
        }
        if (searched != null && category == null && author != null && status == null) {
            ArrayList<Post> post = postDB.searchPost2(searched, author);
            req.setAttribute("post", post);
        }
        if (searched != null && category == null && author == null && status != null) {
            ArrayList<Post> post = postDB.searchPost3(searched, status);
            req.setAttribute("post", post);
        }
        if (searched != null && category != null && author != null && status == null) {
            ArrayList<Post> post = postDB.searchPost4(searched, category, author);
            req.setAttribute("post", post);
        }
        if (searched != null && category != null && author == null && status != null) {
            ArrayList<Post> post = postDB.searchPost5(searched, category, status);
            req.setAttribute("post", post);
        }
        if (searched != null && category == null && author != null && status != null) {
            ArrayList<Post> post = postDB.searchPost6(searched, author, status);
            req.setAttribute("post", post);
        }
        if (searched == null && category != null && author != null && status == null) {
            ArrayList<Post> post = postDB.searchPost7(category, author);
            req.setAttribute("post", post);
        }
        if (searched == null && category != null && author == null && status != null) {
            ArrayList<Post> post = postDB.searchPost8(category, status);
            req.setAttribute("post", post);
        }
        if (searched == null && category != null && author != null && status != null) {
            ArrayList<Post> post = postDB.searchPost9(category, author, status);
            req.setAttribute("post", post);
        }
        if (searched == null && category == null && author != null && status != null) {
            ArrayList<Post> post = postDB.searchPost10(author, status);
            req.setAttribute("post", post);
        }
        
//        String title = req.getParameter("title");
//        boolean flag = req.getParameter("flag").equals("On");
//        
//        Post p = new Post();
//        p.setTitle(title);
//        p.setFlag(flag);
//        
//        postDB.editFlag(p);
        req.getRequestDispatcher("/post/list.jsp").forward(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
