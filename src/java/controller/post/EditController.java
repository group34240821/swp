/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.post;

import context.CategoryDAO;
import context.PostDAO;
import java.io.IOException;
//import jakarta.servlet.ServletException;
//import jakarta.servlet.http.HttpServlet;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import model.Category;
import model.Post;

/**
 *
 * @author Mạc Huyền
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 5, // 5MB
        maxRequestSize = 1024 * 1024 * 50)
public class EditController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int postid = Integer.parseInt(req.getParameter("postid"));
        PostDAO postDB = new PostDAO();
        Post post = postDB.get(postid);

        CategoryDAO categoryDB = new CategoryDAO();
        ArrayList<Category> category = categoryDB.list();

//        UserDAO authorDB = new UserDAO();
//        ArrayList<User> author = authorDB.listAuthor();
        req.setAttribute("post", post);
        req.setAttribute("category", category);
//        req.setAttribute("author", author);
        req.getRequestDispatcher("/post/post-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String postid = req.getParameter("postid");
        String title = req.getParameter("title");
        String category_id = req.getParameter("category");
        String summary = req.getParameter("summary");
        String content = req.getParameter("content");
        String status = req.getParameter("status");
        String flag = req.getParameter("flag");

        Part file = req.getPart("img");
        String fileName = extractFileName(file);  // get selected image file name
        if (fileName.equals("")) {
            Category c = new Category();
            c.setCategory_id(Integer.parseInt(category_id));

            Post p = new Post();
            p.setPostid(Integer.parseInt(postid));
            p.setTitle(title);
            p.setCate(c);
            p.setThumbnail(fileName);
            p.setSummary(summary);
            p.setContent(content);
            p.setStatus(status);
            p.setFlag(flag);

            PostDAO postDB = new PostDAO();
            postDB.editWithoutImg(p);
            postDB.editFlag(p);
        } else {
            String applicationPath = getServletContext().getRealPath("");
            String uploadPath = applicationPath + "assets\\img\\blog";
            File fileUploadDirectory = new File(uploadPath);
            if (!fileUploadDirectory.exists()) {
                fileUploadDirectory.mkdirs();
            }
            String savePath = uploadPath + File.separator + fileName;
            file.write(savePath + File.separator);

            Category c = new Category();
            c.setCategory_id(Integer.parseInt(category_id));

            Post p = new Post();
            p.setPostid(Integer.parseInt(postid));
            p.setTitle(title);
            p.setCate(c);
            p.setThumbnail(fileName);
            p.setSummary(summary);
            p.setContent(content);
            p.setStatus(status);
            p.setFlag(flag);

            PostDAO postDB = new PostDAO();
            postDB.edit(p);
            postDB.editFlag(p);
        }
        resp.sendRedirect("post-list");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String extractFileName(Part file) {
        String contentDisp = file.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

}
