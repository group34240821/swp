/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Order;
import model.User1;

/**
 *
 * @author BUI QUOC BAO
 */
public class OrderDAO extends DBContext{
    public ArrayList<Order> list(){
        ArrayList<Order> order = new ArrayList<>();
        try {
            String sql = "delect * from `swp391`.`order`";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                order.add(new Order(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getTimestamp(4), rs.getDouble(5)));
            }
        } catch (Exception e) {
        }
        
        return order;
    }
    
    public  ArrayList<Order> pagging(int index){
        ArrayList<Order> order = new ArrayList<>();
        try {
            String sql = "select * from `swp391`.`order` order by order_id limit 5 offset ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, (index-1)*5);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                order.add(new Order(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getTimestamp(4), rs.getDouble(5)));
            }
        } catch (Exception e) {
        }
        return order;
    }
    
    public int count(){
        try {
            String sql = "select count(*) from `swp391`.`order`";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }
    public ArrayList<Order> getbyID(int order_id){
        ArrayList<Order> order = new ArrayList<>();
        String sql = "select * from 'swp391`.`order` where `order_id` = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, order_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                order.add(new Order(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getTimestamp(4), rs.getDouble(5)));
            }
        } catch (Exception e) {
        }
        return order;
    }
    public int getUID(int order_id){
        try {
            String sql = "select `order`.`user_id` from `swp391`.`order` where `order_id` = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }
    
    public ArrayList<User1> getByID(int user_id){
        ArrayList<User1> u = new ArrayList<>();
        String sql = "select * from `swp391`.`user` where user_id = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, user_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                u.add(new User1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return u;
    }
    
    public ArrayList<User1> getAll(){
        ArrayList<User1> u = new ArrayList<>();
        String sql = "select * from `swp391`.`user`";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                u.add(new User1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return u;
    }
    
}
