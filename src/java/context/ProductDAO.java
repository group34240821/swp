/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Brand;
import model.Product;
import model.category1;
import model.product1;

/**
 *
 * @author DW
 */
public class ProductDAO extends DBContext {
//    SELECT p.product_id , p.product_name , p.price , p.sale , p.description , p.pimg_url , p.brand_id , p.category_id , p.createdDate,
//p.createdBy , p.modifiedDate , p.modifiedBy , p.isDelete , p.status , SUM(od.quantity) AS totalQuantity
//FROM order_detail od
//JOIN product p ON od.product_id  = p.product_id 
//JOIN swp391.order o ON od.order_id  = o.order_id 
//WHERE o.createDate >= DATE(:startDate) AND o.createDate <= DATE(:endDate)
//GROUP By p.product_id , p.product_name 
//ORDER BY totalQuantity DESC;

    public ArrayList<Product> paggingProduct(String startDate, String endDate, int pageindex, int pagesize) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            String sql = "SELECT p.product_id , p.product_name , p.price , p.sale , p.description , p.pimg_url , p.brand_id , p.category_id , p.createdDate,\n"
                    + "p.createdBy , p.modifiedDate , p.modifiedBy , p.isDelete , p.status , SUM(od.quantity) AS totalQuantity\n"
                    + "FROM order_detail od\n"
                    + "JOIN product p ON od.product_id  = p.product_id \n"
                    + "JOIN swp391.order o ON od.order_id  = o.order_id \n"
                    + "WHERE o.createdDate >= DATE(?) AND o.createdDate <= DATE(?)\n"
                    + "GROUP By p.product_id , p.product_name \n"
                    + "ORDER BY totalQuantity DESC \n"
                    + "LIMIT ?, ? ;";
            PreparedStatement stm = connection.prepareStatement(sql);
            int offset = (pageindex - 1) * pagesize;
            stm.setString(1, startDate);
            stm.setString(2, endDate);
            stm.setInt(3, offset);
            stm.setInt(4, pagesize);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {

                int productId = rs.getInt("product_id");
                String productName = rs.getString("product_name");
                double price = rs.getDouble("price");
                int sale = rs.getInt("sale");
                String description = rs.getString("description");
                String img = rs.getString("pimg_url");
                int brandId = rs.getInt("brand_id");
                int cateId = rs.getInt("category_id");
                Date createDate = rs.getDate("createdDate");
                int createBy = rs.getInt("createdBy");
                Date modifyDate = rs.getDate("modifiedDate");
                int modifyBy = rs.getInt("modifiedBy");
                int isDelete = rs.getInt("isDelete");
                String status = rs.getString("status");

                Product product = new Product(productId, productName, price, sale,
                        description, img, brandId, cateId, createDate, createBy, modifyDate, modifyBy,
                        isDelete, status);

                products.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }
    
    
    
    public ArrayList<product1> pagging(int index){
        ArrayList<product1> product = new ArrayList<>();
        try {
            String sql ="select * from `swp391`.`product` order by product_id limit 5 offset ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, (index-1)*5);
            ResultSet rs = stm.executeQuery();       
            while (rs.next()) {                
                product.add(new product1(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(14)));
            }
        } catch (Exception e) {
        }
        return product;
    }
    
    public int count(){
        try {
            String sql = "select count(*) from `swp391`.`product`";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }
     
     public void delete(int product_id){
         String sql = "delete from `swp391`.`product` where product_id = ?";
         try {
             PreparedStatement stm = connection.prepareStatement(sql);
             stm.setInt(1, product_id);
             stm.executeUpdate();
         } catch (Exception e) {
         }
     }
     
     public void update(int product_id, String product_name, double price, double sale, String description, String pimg_url, int brand_id, int category_id, String status){
         String sql = "update `swp391`.`product` set product_name = ?, price = ?,sale =?, description = ?, pimg_url = ?, brand_id = ?, category_id =?, status = ? where product_id = ?";
         try {
             PreparedStatement stm = connection.prepareStatement(sql);
             stm.setInt(9, product_id);
             stm.setInt(6, brand_id);
             stm.setInt(7, category_id);
             stm.setString(1, product_name);
             stm.setString(4, description);
             stm.setString(5, pimg_url);
             stm.setString(8, status);
             stm.setDouble(2, price);
             stm.setDouble(3, sale);
             stm.executeUpdate();
         } catch (Exception e) {
         }
     }
     
    public ArrayList<product1> getByID( int product_id){
        ArrayList<product1> p = new ArrayList<>();
        String sql = "select * from `swp391`.`product` where product_id = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, product_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                p.add(new product1(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(14)));
            }
        } catch (Exception e) {
        }
        return p;
    }
    
    public  ArrayList<product1> search(String product_name){
        ArrayList<product1> p = new ArrayList<>();
        String sql = "SELECT * FROM swp391.product where product_name like '%"+product_name+"%';";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                p.add(new product1(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(14)));
            }
        } catch (Exception e) {
        }
        return p;
    }
    
    public ArrayList<category1> getallC(){
        ArrayList<category1> c = new ArrayList<>();
        try {
            String sql = "select * from `swp391`.`category`";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                c.add(new category1(rs.getInt(1),rs.getString(2)));
            }
        } catch (Exception e) {
        }
        return c;
    }
    
    public  ArrayList<product1> getbycate(int category_id){
        ArrayList<product1> p = new ArrayList<>();
        String sql = "select * from `swp391`.`product` where category_id = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, category_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                p.add(new product1(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(14)));
            }
        } catch (Exception e) {
        }
        return p;
    }
    
    public ArrayList<Brand> getallB(){
        ArrayList<Brand> b = new ArrayList<>();
        try {
            String sql = "select * from `swp391`.`brand`";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                b.add(new Brand(rs.getInt(1),rs.getString(2)));
            }
        } catch (Exception e) {
        }
        return b;
    }
    
    public ArrayList<product1> getbyBrand(int brand_id){
        ArrayList<product1> p = new ArrayList<>();
        String sql = "select * from `swp391`.`product` where brand_id = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, brand_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                p.add(new product1(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(14)));
            }
        } catch (Exception e) {
        }
        return p;
    } 
    
}
