/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User1;

/**
 *
 * @author BUI QUOC BAO
 */
public class UserDAO2 extends DBContext {

    public User1 checkAuth(String email, String password) throws SQLException {
        String sql = "SELECT `user`.`user_id`,\n"
                + "    `user`.`user_name`,\n"
                + "    `user`.`password`,\n"
                + "    `user`.`full_name`,\n"
                + "    `user`.`gender`,\n"
                + "    `user`.`email`,\n"
                + "    `user`.`mobile`,\n"
                + "    `user`.`address`,\n"
                + "    `user`.`role_id`,\n"
                + "    `user`.`status`\n"
                + "FROM `swp391`.`user` where email = '" + email + "' and password = '" + password + "'";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User1 user = new User1();
                user.setId(rs.getInt("user_id"));
                user.setUsername(rs.getString("user_name"));
                user.setPassword(password);
                user.setFull_name(rs.getString("full_name"));
                user.setGender(rs.getString("gender"));
                user.setEmail(email);
                user.setMobile(rs.getString("mobile"));
                user.setAddress(rs.getString("address"));
                user.setRoleId(rs.getInt("role_id"));
                user.setStatus(rs.getString("status"));
                return user;
            }
        } catch (SQLException e) {
        }

        return null;
    }

    public User1 getUser(String email) {
        String sql = "SELECT `user`.`user_id`,\n"
                + "    `user`.`user_name`,\n"
                + "    `user`.`password`,\n"
                + "    `user`.`Full_name`,\n"
                + "    `user`.`gender`,\n"
                + "    `user`.`email`,\n"
                + "    `user`.`mobile`,\n"
                + "    `user`.`address`,\n"
                + "    `user`.`role_id`,\n"
                + "    `user`.`status`\n"
                + "FROM `swp391`.`user` where email = '" + email + "'";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User1 user = new User1();
                user.setId(rs.getInt("user_id"));
                user.setUsername(rs.getString("user_name"));
                user.setPassword(rs.getString("password"));
                user.setFull_name(rs.getString("full_name"));
                user.setEmail(rs.getString("email"));
                user.setMobile(rs.getString("mobile"));
                user.setAddress(rs.getString("address"));
                user.setRoleId(rs.getInt("role_id"));
                user.setStatus(rs.getString("status"));
                return user;
            }
        } catch (SQLException e) {
        }

        return null;
    }

    public User1 updateProfile(int id , String username, String fullname, String mobile, String address) {
        try {
        String sql = "UPDATE `swp391`.`user`\n"
                + "SET\n"
                + "`user_name` = ?,\n"
                + "`full_name` = ?,\n"
                + "`mobile` = ?,\n"
                + "`address` = ?\n"
                + "WHERE `user_id` = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                User1 ur = new User1();
                st.setString(1, username);
                st.setString(2, fullname);
                st.setString(3, mobile);
                st.setString(4, address);
                st.setInt(5, id);
                st.executeUpdate();
            
        } catch (SQLException e) {
        }
        return null;
    }
    
    public static void UpdatePassUser(String resetPass, int id) {
        String sql = "update user set password = ? where user_id = ?";
        try {
            Connection conn = new DBContext().connection;
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, resetPass);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }
    
    public static User1 checkAuthByEmail(String email) throws SQLException {
        String sql = "SELECT `user`.`user_id`,\n"
                + "    `user`.`user_name`,\n"
                + "    `user`.`email`,\n"
                + "    `user`.`password`,\n"
                + "    `user`.`mobile`,\n"
                + "    `user`.`address`,\n"
                + "    `user`.`role_id`,\n"
                + "    `user`.`status`\n"
                + "FROM `swp391`.`user` where email = '" + email+"'";
        try {
            Connection conn = new DBContext().connection;
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User1 user = new User1();
                user.setId(rs.getInt("user_id"));
                user.setUsername(rs.getString("user_name"));
                user.setEmail(email);
                user.setPassword(rs.getString("password"));
                user.setMobile(rs.getString("mobile"));
                user.setAddress(rs.getString("address"));
                user.setRoleId(rs.getInt("role_id"));
                user.setStatus(rs.getString("status"));
                return user;
            }
        } catch (SQLException e) {
        }

        return null;
    }
    
}
