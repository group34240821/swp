/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Role;

/**
 *
 * @author Mạc Huyền
 */
public class RoleDAO extends DBContext {
    ArrayList<Role> role = new ArrayList<>();

    /*
     * HuyenPTN - 25/6/2023
     * List all role
     */
    public ArrayList<Role> list() {
        try {
            String sql = "SELECT * FROM swp391.role";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                role.add(r);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RoleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return role;
    }
    
}
