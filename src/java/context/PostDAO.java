/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;
import model.Post;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
public class PostDAO extends DBContext {
    
    /*
     * HuyenPTN - 25/6/2023
     * List all post with pagging
     */
    public ArrayList<Post> pagging(int pageindex, int pagesize) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.summary, "
                    + "b.content, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "ORDER BY b.update_date DESC\n"
                    + "LIMIT ?, ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            int offset = (pageindex - 1) * pagesize;
            stm.setInt(1, offset);
            stm.setInt(2, pagesize);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setSummary(rs.getString("summary"));
                p.setContent(rs.getString("content"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }
    
    /*
     * HuyenPTN - 25/6/2023
     * Count the number of posts 
     */
    public int count() {
        try {
            String sql = "SELECT count(*) AS total FROM swp391.blog";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search posts by title
     */
    public ArrayList<Post> searchByText(String searched) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.title LIKE ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search posts by category
     */
    public ArrayList<Post> searchByCategory(String category) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.category_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, category);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search posts by author
     */
    public ArrayList<Post> searchByAuthor(String author) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.author = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, author);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search posts by status
     */
    public ArrayList<Post> searchByStatus(String status) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by title and category
     */
    public ArrayList<Post> searchPost1(String searched, String category) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.title LIKE ? AND b.category_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, category);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by title and author
     */
    public ArrayList<Post> searchPost2(String searched, String author) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.title LIKE ? AND b.author = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, author);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by title and status
     */
    public ArrayList<Post> searchPost3(String searched, String status) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.title LIKE ? AND b.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by title, category and author
     */
    public ArrayList<Post> searchPost4(String searched, String category, String author) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.title LIKE ? AND b.category_id = ? AND b.author = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, category);
            stm.setString(3, author);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by title, category and status
     */
    public ArrayList<Post> searchPost5(String searched, String category, String status) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.title LIKE ? AND b.category_id = ? AND b.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, category);
            stm.setString(3, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by title, author and status
     */
    public ArrayList<Post> searchPost6(String searched, String author, String status) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.title LIKE ? AND b.author = ? AND b.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, author);
            stm.setString(3, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by category and author
     */
    public ArrayList<Post> searchPost7(String category, String author) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.category_id = ? AND b.author = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, category);
            stm.setString(2, author);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by category and status
     */
    public ArrayList<Post> searchPost8(String category, String status) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.category_id = ? AND b.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, category);
            stm.setString(2, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by category, author and status
     */
    public ArrayList<Post> searchPost9(String category, String author, String status) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.category_id = ? AND b.author = ? AND b.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, category);
            stm.setString(2, author);
            stm.setString(3, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by author and status
     */
    public ArrayList<Post> searchPost10(String author, String status) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.author = ? AND b.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, author);
            stm.setString(2, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search post by title, category, author and status
     */
    public ArrayList<Post> searchPost11(String searched, String category, String author, String status) {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id, \n"
                    + "c.category_name, b.author, u.user_id, u.full_name, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.title LIKE ? AND b.category_id = ? "
                    + "AND b.author = ? AND b.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, category);
            stm.setString(3, author);
            stm.setString(4, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setThumbnail(rs.getString("thumbnail_url"));
                p.setTitle(rs.getString("title"));
                p.setStatus(rs.getString("status"));
                p.setFlag(rs.getString("flag"));

                Category c = new Category();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));
                c.setPost(p);
                p.getCategory().add(c);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setPosts(p);
                p.getAuthor().add(u);

                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Edit flag Show/Hide
     */
    public void editFlag(Post post) {
        try {
            connection.setAutoCommit(false);
            String sql = "UPDATE swp391.blog\n"
                    + "SET blog_id = ?, flag = ?\n"
                    + "WHERE blog_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, post.getPostid());
            stm.setString(2, post.getFlag());
            stm.setInt(3, post.getPostid());
            stm.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
     * HuyenPTN - 25/6/2023
     * Get information of the post selected by postid
     */
    public Post get(int postid) {
        try {
            String sql = "SELECT b.blog_id, b.thumbnail_url, b.title, b.category_id,\n"
                    + "c.category_name, u.user_id, u.full_name, b.summary, b.content, b.status, b.flag\n"
                    + "FROM swp391.blog b INNER JOIN swp391.category c\n"
                    + "ON b.category_id = c.category_id\n"
                    + "INNER JOIN swp391.user u ON b.author = u.user_id\n"
                    + "WHERE b.blog_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, postid);
            ResultSet rs = stm.executeQuery();
            Post p = null;
            while (rs.next()) {
                if (p == null) {
                    p = new Post();
                    p.setPostid(rs.getInt("blog_id"));
                    p.setThumbnail(rs.getString("thumbnail_url"));
                    p.setTitle(rs.getString("title"));
                    p.setSummary(rs.getString("summary"));
                    p.setContent(rs.getString("content"));
                    p.setStatus(rs.getString("status"));
                    p.setFlag(rs.getString("flag"));

                    Category c = new Category();
                    c.setCategory_id(rs.getInt("category_id"));
                    c.setCategory_name(rs.getString("category_name"));
                    c.setPost(p);
                    p.setCate(c);
                    p.getCategory().add(c);

                    User u = new User();
                    u.setUserid(rs.getInt("user_id"));
                    u.setFullname(rs.getString("full_name"));
                    u.setPosts(p);
                    p.setAuth(u);
                    p.getAuthor().add(u);
                }
            }
            return p;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Add a new post information 
     */
    public void create(Post post) {
        try {
            connection.setAutoCommit(false);
            String sql = "INSERT INTO swp391.blog\n"
                    + "(title, category_id, author, thumbnail_url, summary, content, status)\n"
                    + "VALUE (?, ?, 2, ?, ?, ?, 'Pending')";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, post.getTitle());
            stm.setInt(2, post.getCate().getCategory_id());
            stm.setString(3, post.getThumbnail());
            stm.setString(4, post.getSummary());
            stm.setString(5, post.getContent());
            stm.executeUpdate();

            String sql_get_userid = "SELECT @@IDENTITY AS blog_id";
            PreparedStatement stm_get_userid = connection.prepareStatement(sql_get_userid);
            ResultSet rs = stm_get_userid.executeQuery();
            if (rs.next()) {
                post.setPostid(rs.getInt("blog_id"));
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
     * HuyenPTN - 25/6/2023
     * Edit all information of the post selected
     */
    public void edit(Post post) {
        try {
            connection.setAutoCommit(false);
            String sql = "UPDATE swp391.blog\n"
                    + "SET blog_id = ?, title = ?, category_id = ?, \n"
                    + "thumbnail_url = ?, summary = ?, content = ?, status = ?\n"
                    + "WHERE blog_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, post.getPostid());
            stm.setString(2, post.getTitle());
            stm.setInt(3, post.getCate().getCategory_id());
            stm.setString(4, post.getThumbnail());
            stm.setString(5, post.getSummary());
            stm.setString(6, post.getContent());
            stm.setString(7, post.getStatus());
            stm.setInt(8, post.getPostid());
            stm.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
     * HuyenPTN - 25/6/2023
     * Edit all information except thumbnail of the post selected 
     */
    public void editWithoutImg(Post post) {
        try {
            connection.setAutoCommit(false);
            String sql = "UPDATE swp391.blog\n"
                    + "SET blog_id = ?, title = ?, category_id = ?, \n"
                    + "summary = ?, content = ?, status = ?\n"
                    + "WHERE blog_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, post.getPostid());
            stm.setString(2, post.getTitle());
            stm.setInt(3, post.getCate().getCategory_id());
            stm.setString(4, post.getSummary());
            stm.setString(5, post.getContent());
            stm.setString(6, post.getStatus());
            stm.setInt(7, post.getPostid());
            stm.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public ArrayList<Post> recentPost() {
        ArrayList<Post> post = new ArrayList<>();
        try {
            String sql = "SELECT blog_id, title, update_date FROM swp391.blog\n"
                    + "ORDER BY update_date DESC LIMIT 4";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Post p = new Post();
                p.setPostid(rs.getInt("blog_id"));
                p.setTitle(rs.getString("title"));
                p.setUpdate_date(rs.getDate("update_date"));
                post.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }
}
