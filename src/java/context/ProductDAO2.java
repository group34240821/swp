/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product2;
import model.Category2;

/**
 *
 * @author Cao Gia Linh
 */
public class ProductDAO2 extends DBContext {

    //load danh sach Product tu db 
    public List<Product2> GetProductList() {
        List<Product2> product = new ArrayList<>();
        try {
            String sql = "SELECT * FROM swp391.product\n"
                    + " order by modifiedDate ;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product2 p = new Product2();
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setSale(rs.getDouble("sale"));
                p.setDescription(rs.getString("description"));
                p.setPimg_url(rs.getString("pimg_url"));
                p.setBrand_id(rs.getInt("brand_id"));
                p.setCategory_id(rs.getInt("category_id"));
//                p.setCreatedDate(rs.getDate("createdDate"));
//                p.setCreatedBy(rs.getInt("createdBy"));
//                p.setModifiedDate(rs.getDate("modifiedDate"));
//                p.setModifieBy(rs.getInt("modifieBy"));
//                p.setIsDelete(rs.getBoolean("isDelete"));
                p.setStatus(rs.getString("status"));

                product.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

    public List<Category2> GetCategoryList() {
        List<Category2> cate = new ArrayList<>();
        try {
            String sql = "SELECT * FROM swp391.category;";

            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Category2 c = new Category2();
                c.setCategory_id(rs.getInt("category_id"));
                c.setCategory_name(rs.getString("category_name"));

                cate.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cate;
    }

    public List<Product2> SearchProductByPName(String txtSearch) {
        List<Product2> product = new ArrayList<>();
        try {
            String sql = "SELECT * FROM swp391.product where product_name like ? "
                    + "order by modifiedDate desc  ;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + txtSearch + "%");
            //stm.setInt(2, (index - 1) * 6);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product2 p = new Product2();
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setSale(rs.getDouble("sale"));
                p.setDescription(rs.getString("description"));
                p.setPimg_url(rs.getString("pimg_url"));
                p.setBrand_id(rs.getInt("brand_id"));
                p.setCategory_id(rs.getInt("category_id"));
//                p.setCreatedDate(rs.getDate("createdDate"));
//                p.setCreatedBy(rs.getInt("createdBy"));
//                p.setModifiedDate(rs.getDate("modifiedDate"));
//                p.setModifieBy(rs.getInt("modifieBy"));
//                p.setIsDelete(rs.getBoolean("isDelete"));
                p.setStatus(rs.getString("status"));
                product.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

//
    public List<Product2> SearchProductByCId(String category_id) {
        List<Product2> product = new ArrayList<>();
        try {
            String sql = "SELECT product_id, product_name, price, sale,\n"
                    + "`description`, pimg_url, brand_id, category_id,createdDate, \n "
                    + "`createdBy, modifiedDate,modifiedBy,isDelete,`status` \n "
                    + " FROM swp391.product WHERE category_id = ?"
                    + " ORDER BY modifiedDate desc \n ";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, category_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product2 p = new Product2();
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setSale(rs.getDouble("sale"));
                p.setDescription(rs.getString("description"));
                p.setPimg_url(rs.getString("pimg_url"));
                p.setBrand_id(rs.getInt("brand_id"));
                p.setCategory_id(rs.getInt("category_id"));
//                p.setCreatedDate(rs.getDate("createdDate"));
//                p.setCreatedBy(rs.getInt("createdBy"));
//                p.setModifiedDate(rs.getDate("modifiedDate"));
//                p.setModifieBy(rs.getInt("modifieBy"));
//                p.setIsDelete(rs.getBoolean("isDelete"));
                p.setStatus(rs.getString("status"));
                product.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

        public List<Product2> getByID(int product_id) {
        List<Product2> product = new ArrayList<>();
        String sql = "select * from `swp391`.`product` where product_id = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, product_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product2 p = new Product2();
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setSale(rs.getDouble("sale"));
                p.setDescription(rs.getString("description"));
                p.setPimg_url(rs.getString("pimg_url"));
                p.setBrand_id(rs.getInt("brand_id"));
                p.setCategory_id(rs.getInt("category_id"));
//                p.setCreatedDate(rs.getDate("createdDate"));
//                p.setCreatedBy(rs.getInt("createdBy"));
//                p.setModifiedDate(rs.getDate("modifiedDate"));
//                p.setModifieBy(rs.getInt("modifieBy"));
//                p.setIsDelete(rs.getBoolean("isDelete"));
                p.setStatus(rs.getString("status"));

                product.add(p);
            }
        } catch (Exception e) {
        }
        return product;
    }   

    public int count() {
        try {
            String sql = "select count(*) from `swp391`.`product`";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int countSearch(String txtSearch) {
        try {
            String sql = "select count(*) from `swp391`.`product` where product_name like ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + txtSearch + "%");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Product2> pagging(int index) {
        List<Product2> product = new ArrayList<>();
        try {
            String sql = "select * from `swp391`.`product` order by modifiedDate desc limit 6 offset ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, (index - 1) * 6);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product2 p = new Product2();
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setSale(rs.getDouble("sale"));
                p.setDescription(rs.getString("description"));
                p.setPimg_url(rs.getString("pimg_url"));
                p.setBrand_id(rs.getInt("brand_id"));
                p.setCategory_id(rs.getInt("category_id"));
//                p.setCreatedDate(rs.getDate("createdDate"));
//                p.setCreatedBy(rs.getInt("createdBy"));
//                p.setModifiedDate(rs.getDate("modifiedDate"));
//                p.setModifieBy(rs.getInt("modifieBy"));
//                p.setIsDelete(rs.getBoolean("isDelete"));
                p.setStatus(rs.getString("status"));
                product.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

//--------------------------------Sort----------------------------------------------
//low to high
    public List<Product2> SortProductListByPrice() {
        List<Product2> product = new ArrayList<>();
        try {
            String sql = "SELECT * FROM swp391.product\n"
                    + " order by sale ;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product2 p = new Product2();
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setSale(rs.getDouble("sale"));
                p.setDescription(rs.getString("description"));
                p.setPimg_url(rs.getString("pimg_url"));
                p.setBrand_id(rs.getInt("brand_id"));
                p.setCategory_id(rs.getInt("category_id"));
//                p.setCreatedDate(rs.getDate("createdDate"));
//                p.setCreatedBy(rs.getInt("createdBy"));
//                p.setModifiedDate(rs.getDate("modifiedDate"));
//                p.setModifieBy(rs.getInt("modifieBy"));
//                p.setIsDelete(rs.getBoolean("isDelete"));
                p.setStatus(rs.getString("status"));

                product.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

//high to low
    public List<Product2> SortProductListByPriceDown() {
        List<Product2> product = new ArrayList<>();
        try {
            String sql = "SELECT * FROM swp391.product\n"
                    + " order by sale desc;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product2 p = new Product2();
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setSale(rs.getDouble("sale"));
                p.setDescription(rs.getString("description"));
                p.setPimg_url(rs.getString("pimg_url"));
                p.setBrand_id(rs.getInt("brand_id"));
                p.setCategory_id(rs.getInt("category_id"));
//                p.setCreatedDate(rs.getDate("createdDate"));
//                p.setCreatedBy(rs.getInt("createdBy"));
//                p.setModifiedDate(rs.getDate("modifiedDate"));
//                p.setModifieBy(rs.getInt("modifieBy"));
//                p.setIsDelete(rs.getBoolean("isDelete"));
                p.setStatus(rs.getString("status"));

                product.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

//Z to A
    public List<Product2> SortProductListByName() {
        List<Product2> product = new ArrayList<>();
        try {
            String sql = "SELECT * FROM swp391.product\n"
                    + " order by product_name desc;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product2 p = new Product2();
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setSale(rs.getDouble("sale"));
                p.setDescription(rs.getString("description"));
                p.setPimg_url(rs.getString("pimg_url"));
                p.setBrand_id(rs.getInt("brand_id"));
                p.setCategory_id(rs.getInt("category_id"));
//                p.setCreatedDate(rs.getDate("createdDate"));
//                p.setCreatedBy(rs.getInt("createdBy"));
//                p.setModifiedDate(rs.getDate("modifiedDate"));
//                p.setModifieBy(rs.getInt("modifieBy"));
//                p.setIsDelete(rs.getBoolean("isDelete"));
                p.setStatus(rs.getString("status"));

                product.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

//A to Z
    public List<Product2> SortProductListByNameDown() {
        List<Product2> product = new ArrayList<>();
        try {
            String sql = "SELECT * FROM swp391.product\n"
                    + " order by product_name ;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product2 p = new Product2();
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setSale(rs.getDouble("sale"));
                p.setDescription(rs.getString("description"));
                p.setPimg_url(rs.getString("pimg_url"));
                p.setBrand_id(rs.getInt("brand_id"));
                p.setCategory_id(rs.getInt("category_id"));
//                p.setCreatedDate(rs.getDate("createdDate"));
//                p.setCreatedBy(rs.getInt("createdBy"));
//                p.setModifiedDate(rs.getDate("modifiedDate"));
//                p.setModifieBy(rs.getInt("modifieBy"));
//                p.setIsDelete(rs.getBoolean("isDelete"));
                p.setStatus(rs.getString("status"));

                product.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }
}
