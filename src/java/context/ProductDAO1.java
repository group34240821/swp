/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.PreparedStatement;

/**
 *
 * @author BUI QUOC BAO
 */
public class ProductDAO1 extends DBContext{
    public void add( String product_name, double price, double sale, String description, String pimg_url, int brand_id, int category_id, String status){
         String sql = "insert into `swp391`.`product` ( product_name,price,sale,description,pimg_url,brand_id,category_id,status) values (?,?,?,?,?,?,?,?)";
         try {
             PreparedStatement stm = connection.prepareStatement(sql);
             stm.setString(1, product_name);
             stm.setDouble(2, price);
             stm.setDouble(3, sale);
             stm.setString(4, description);
             stm.setString(5, pimg_url);
             stm.setInt(6, brand_id);
             stm.setInt(7, category_id);
             stm.setString(8, status);
             stm.executeUpdate();
         } catch (Exception e) {
         }
     }
}
