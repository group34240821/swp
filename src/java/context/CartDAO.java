/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cart;
import model.Product2;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
public class CartDAO extends DBContext {

    ArrayList<Cart> cart = new ArrayList<>();

    public ArrayList<Cart> getCart(int userid) {
        try {
            String sql = "SELECT c.cart_id, p.pimg_url, c.product_id, p.product_name, \n"
                    + "p.price, c.quantity, c.user_id\n"
                    + "FROM swp391.cart c INNER JOIN swp391.product p \n"
                    + "ON c.product_id = p.product_id\n"
                    + "INNER JOIN swp391.user u ON c.user_id = u.user_id\n"
                    + "WHERE c.user_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Cart c = new Cart();
                c.setCartid(rs.getInt("cart_id"));
                c.setQuantity(rs.getInt("quantity"));

                Product2 p = new Product2();
                p.setPimg_url(rs.getString("pimg_url"));
                p.setProduct_id(rs.getInt("product_id"));
                p.setProduct_name(rs.getString("product_name"));
                p.setPrice(rs.getDouble("price"));
                p.setCart(c);
                c.getProduct().add(p);

                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setCart(c);
                c.getUser().add(u);

                cart.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cart;
    }

    public void removeCart(Cart cart) {
        try {
            connection.setAutoCommit(false);
            String sql = "DELETE FROM swp391.cart WHERE cart_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cart.getCartid());
            stm.executeUpdate();
            //success
            connection.commit();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void changeQuantity(Cart cart) {
        try {
            connection.setAutoCommit(false);
            String sql = "UPDATE swp391.cart SET quantity = ?\n"
                    + "WHERE cart_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cart.getQuantity());
            stm.setInt(2, cart.getCartid());
            stm.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
