/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Role;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
public class CustomerDAO extends DBContext {

    /*
     * HuyenPTN - 25/6/2023
     * List all customer
     */
    public ArrayList<User> list() {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT user_id, full_name, gender, email,\n"
                    + "mobile, role_id, status, img_url\n"
                    + "FROM swp391.user WHERE role_id = 4";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setStatus(rs.getString("status"));
                u.setImg(rs.getString("img_url"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setUser(u);
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * List all customer with pagging
     */
    public ArrayList<User> pagging(int pageindex, int pagesize) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT user_id, full_name, gender, \n"
                    + "email, mobile, role_id, status\n"
                    + "FROM swp391.user WHERE role_id = 4\n"
                    + "ORDER BY user_id ASC LIMIT ?, ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            int offset = (pageindex - 1) * pagesize;
            stm.setInt(1, offset);
            stm.setInt(2, pagesize);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setUser(u);
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Count the number of customers (the user with role_id = 4
     */
    public int count() {
        try {
            String sql = "SELECT count(*) AS total FROM swp391.user WHERE role_id = 4";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
//                User u = new User();
//                Role r = new Role();
//                r.setRoleid(rs.getInt("role_id"));
//                r.setUser(u);
//                u.getRole().add(r);

                return rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Get the customer's information by userid
     */
    public User get(int userid) {
        try {
            String sql = "SELECT user_id, user_name, password, full_name, \n"
                    + "gender, email, mobile, address, status, img_url\n"
                    + "FROM swp391.user WHERE user_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userid);
            ResultSet rs = stm.executeQuery();
            User u = null;
            while (rs.next()) {
                if (u == null) {
                    u = new User();
                    u.setUserid(rs.getInt("user_id"));
                    u.setUsername(rs.getString("user_name"));
                    u.setPassword(rs.getString("password"));
                    u.setFullname(rs.getString("full_name"));
                    u.setGender(rs.getString("gender"));
                    u.setEmail(rs.getString("email"));
                    u.setMobile(rs.getString("mobile"));
                    u.setAddress(rs.getString("address"));
                    u.setStatus(rs.getString("status"));
                    u.setImg(rs.getString("img_url"));
                }
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Edit information of a customer selected
     */
    public void edit(User user) {
        try {
            connection.setAutoCommit(false);
            String sql = "UPDATE swp391.user \n"
                    + "SET user_id = ?, full_name = ?, gender = ?, email = ?, mobile = ?,\n"
                    + "address = ?, status = ?\n"
                    //                    + ", img_url = ?\n"
                    + "WHERE user_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, user.getUserid());
            stm.setString(2, user.getFullname());
            stm.setString(3, user.getGender());
            stm.setString(4, user.getEmail());
            stm.setString(5, user.getMobile());
            stm.setString(6, user.getAddress());
            stm.setString(7, user.getStatus());
//            stm.setString(8, user.getImg());
            stm.setInt(8, user.getUserid());
            stm.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
     * HuyenPTN - 25/6/2023
     * Add customer change history data to the database
     */
    public void insert_changes_history(User user) {
        try {
            connection.setAutoCommit(false);
            String sql = "INSERT INTO swp391.customer_changes \n"
                    + "(user_id, full_name, gender, email, mobile, \n"
                    + "address, status)\n"
                    + "VALUE (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, user.getUserid());
            stm.setString(2, user.getFullname());
            stm.setString(3, user.getGender());
            stm.setString(4, user.getEmail());
            stm.setString(5, user.getMobile());
            stm.setString(6, user.getAddress());
            stm.setString(7, user.getStatus());
//            stm.setInt(6, user.getUpdate_by());
            stm.executeUpdate();

//            String sql_get_userid = "SELECT @@IDENTITY AS change_id";
//            PreparedStatement stm_get_userid = connection.prepareStatement(sql_get_userid);
//            ResultSet rs = stm_get_userid.executeQuery();
//            if (rs.next()) {
//                user.setUserid(rs.getInt("change_id"));
//            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
     * HuyenPTN - 25/6/2023
     * List all the change history of customer selected
     */
    public ArrayList<User> list_changes_history(int userid) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT user_id, full_name, gender, email, mobile, \n"
                    + "address, status, update_date\n"
                    + "FROM swp391.customer_changes\n"
                    + "WHERE user_id = ?\n"
                    + "ORDER BY update_date DESC";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));
//                u.setUpdate_by(rs.getInt("update_by"));
                u.setUpdate_date(rs.getTimestamp("update_date"));
                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Add a new customer's information
     */
    public void create(User user) {
        try {
            connection.setAutoCommit(false);
            String sql = "INSERT INTO swp391.user \n"
                    + "(password, full_name, gender, \n"
                    + "email, mobile, address, role_id, status, img_url)\n"
                    + "VALUE (?, ?, ?, ?, ?, ?, 4, ?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, user.getPassword());
            stm.setString(2, user.getFullname());
            stm.setString(3, user.getGender());
            stm.setString(4, user.getEmail());
            stm.setString(5, user.getMobile());
            stm.setString(6, user.getAddress());
            stm.setString(7, user.getStatus());
            stm.setString(8, user.getImg());
            stm.executeUpdate();

            String sql_get_userid = "SELECT @@IDENTITY AS user_id";
            PreparedStatement stm_get_userid = connection.prepareStatement(sql_get_userid);
            ResultSet rs = stm_get_userid.executeQuery();
            if (rs.next()) {
                user.setUserid(rs.getInt("user_id"));
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search customer by fullname/email/mobile
     */
    public ArrayList<User> searchByText(String searched) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT user_id, full_name, gender, email, mobile, \n"
                    + "address, role_id, status\n"
                    + "FROM swp391.user WHERE role_id = 4\n"
                    + "AND (full_name LIKE ?\n"
                    + "OR email LIKE ?\n"
                    + "OR mobile LIKE ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search customer by status
     */
    public ArrayList<User> searchByStatus(String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT user_id, full_name, gender, email, mobile, \n"
                    + "address, role_id, status\n"
                    + "FROM swp391.user WHERE role_id = 4\n"
                    + "AND status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search customer by fullname/email/mobile and status
     */
    public ArrayList<User> searchUser1(String searched, String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT user_id, full_name, gender, email, mobile, \n"
                    + "address, role_id, status\n"
                    + "FROM swp391.user WHERE role_id = 4\n"
                    + "AND (full_name LIKE ?\n"
                    + "OR email LIKE ?\n"
                    + "OR mobile LIKE ?)\n"
                    + "AND status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
            stm.setString(4, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    public User getEmail(String email) {
        try {
            String sql = "SELECT email FROM swp391.user WHERE email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            User u = null;
            while (rs.next()) {
                if (u == null) {
                    u = new User();
                    u.setEmail(rs.getString("email"));
                }
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
