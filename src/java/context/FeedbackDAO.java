/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package context;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Feedback;

/**
 *
 * @author Pc
 */
public class FeedbackDAO extends DBContext {

    Connection conn = connection;
    PreparedStatement stm = null;
    ResultSet rs = null;

    private List<Feedback> list;

    public List<Feedback> getList() {
        return list;
    }

    public static void main(String[] args) {
        try {
            FeedbackDAO dao = new FeedbackDAO();
            System.out.println(dao.count());
            int page = dao.count() / 5;
            if ((dao.count() % 5) > 0) {
                page += 1;
            }
            System.out.println("Num A of page totals: " + page);
        } catch (SQLException ex) {
            Logger.getLogger(FeedbackDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int count() throws SQLException {
        String sql = "SELECT COUNT(feedback_id) AS NumberOfFeedbacks FROM swp391.feedback ";
        int count = 0;
        try {
            if (conn != null) {
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                if (rs.next()) {
                    return count = rs.getInt("NumberOfFeedbacks");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
        return count;
    }

    public void getAll(int pagesize, int pagecount) throws SQLException {
        String sql = "SELECT f.feedback_id, u.full_name, p.product_name, p.pimg_url, p.status, f.comment \n"
                + "FROM swp391.feedback f \n"
                + "INNER JOIN swp391.user u ON f.user_id=u.user_id \n"
                + "INNER JOIN swp391.product p ON f.product_id=p.product_id \n"
                + "order by f.feedback_id DESC \n"
                + "limit ? offset ?";
        try {
            if (conn != null) {
                stm = conn.prepareStatement(sql);
                stm.setInt(1, pagesize);
                stm.setInt(2, (pagecount - 1) * pagesize);
                rs = stm.executeQuery();
                while (rs.next()) {
                    int feedback_id = rs.getInt("feedback_id");
                    String full_name = rs.getString("full_name");
                    String product_name = rs.getString("product_name");
                    String product_img = rs.getString("pimg_url");
                    String status = rs.getString("status");
                    String comment = rs.getString("comment");
                    Feedback dto = new Feedback(feedback_id, full_name, product_name, product_img, status, comment);
                    if (this.list == null) {
                        this.list = new ArrayList<>();
                    }
                    this.list.add(dto);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void searchByFullName(String username, int pagesize, int pagecount) throws SQLException {
        String sql = "SELECT f.feedback_id, u.full_name, p.product_name, p.pimg_url, p.status, f.comment \n"
                + "FROM swp391.feedback f \n"
                + "INNER JOIN swp391.user u ON f.user_id=u.user_id \n"
                + "INNER JOIN swp391.product p ON f.product_id=p.product_id \n"
                + "WHERE  u.full_name LIKE ? \n"
                + "order by f.feedback_id DESC \n"
                + "limit ? offset ?";
        try {
            if (conn != null) {
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + username + "%");
                stm.setInt(2, pagesize);
                stm.setInt(3, (pagecount - 1) * pagesize);
                rs = stm.executeQuery();
                while (rs.next()) {
                    int feedback_id = rs.getInt("feedback_id");
                    String full_name = rs.getString("full_name");
                    String product_name = rs.getString("product_name");
                    String product_img = rs.getString("pimg_url");
                    String status = rs.getString("status");
                    String comment = rs.getString("comment");
                    Feedback dto = new Feedback(feedback_id, full_name, product_name, product_img, status, comment);
                    if (this.list == null) {
                        this.list = new ArrayList<>();
                    }
                    this.list.add(dto);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void searchByContent(String content, int pagesize, int pagecount) throws SQLException {
        String sql = "SELECT f.feedback_id, u.full_name, p.product_name, p.pimg_url, p.status, f.comment \n"
                + "FROM swp391.feedback f \n"
                + "INNER JOIN swp391.user u ON f.user_id=u.user_id \n"
                + "INNER JOIN swp391.product p ON f.product_id=p.product_id \n"
                + "WHERE  f.comment LIKE ? \n"
                + "order by f.feedback_id DESC \n"
                + "limit ? offset ?";
        try {
            if (conn != null) {
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + content + "%");
                stm.setInt(2, pagesize);
                stm.setInt(3, (pagecount - 1) * pagesize);
                rs = stm.executeQuery();
                while (rs.next()) {
                    int feedback_id = rs.getInt("feedback_id");
                    String full_name = rs.getString("full_name");
                    String product_name = rs.getString("product_name");
                    String product_img = rs.getString("pimg_url");
                    String status = rs.getString("status");
                    String comment = rs.getString("comment");
                    Feedback dto = new Feedback(feedback_id, full_name, product_name, product_img, status, comment);
                    if (this.list == null) {
                        this.list = new ArrayList<>();
                    }
                    this.list.add(dto);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
}
