/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Role;
import model.User;

/**
 *
 * @author Mạc Huyền
 */
public class UserDAO extends DBContext {

    /*
     * HuyenPTN - 25/6/2023
     * List all user
     */
    public ArrayList<User> list() {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.full_name, u.gender, u.email,\n"
                    + "u.mobile, u.role_id, r.role_name, u.status, u.img_url\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();

                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setStatus(rs.getString("status"));
                u.setImg(rs.getString("img_url"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                r.setUser(u);

                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * List all user with pagging
     */
    public ArrayList<User> pagging(int pageindex, int pagesize) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.full_name, u.gender, u.email,\n"
                    + "u.mobile, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "ORDER BY u.user_id ASC \n"
                    + "LIMIT ?, ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            int offset = (pageindex - 1) * pagesize;
            stm.setInt(1, offset);
            stm.setInt(2, pagesize);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                r.setUser(u);
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Count the number of users
     */
    public int count() {
        try {
            String sql = "SELECT count(*) AS total FROM swp391.user";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Get information of a user selected by userid
     */
    public User get(int userid) {
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status, u.img_url\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE u.user_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userid);
            ResultSet rs = stm.executeQuery();
            User u = null;
            while (rs.next()) {
                if (u == null) {
                    u = new User();
                    u.setUserid(rs.getInt("user_id"));
                    u.setUsername(rs.getString("user_name"));
                    u.setPassword(rs.getString("password"));
                    u.setFullname(rs.getString("full_name"));
                    u.setGender(rs.getString("gender"));
                    u.setEmail(rs.getString("email"));
                    u.setMobile(rs.getString("mobile"));
                    u.setAddress(rs.getString("address"));
                    u.setStatus(rs.getString("status"));
                    u.setImg(rs.getString("img_url"));

                    Role r = new Role();
                    r.setRoleid(rs.getInt("role_id"));
                    r.setRolename(rs.getString("role_name"));
                    u.setRoles(r);
                    u.getRole().add(r);
                }
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Edit information of user selected 
     */
    public void edit(User user) {
        try {
            connection.setAutoCommit(false);
            String sql = "UPDATE swp391.user \n"
                    + "SET user_id = ?,  \n"
                    + "full_name = ?, gender = ?, email = ?, mobile = ?,\n"
                    + "address = ?, role_id = ?, status = ?"
                    //                    + ", img_url = ?\n"
                    + "WHERE user_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, user.getUserid());
            stm.setString(2, user.getFullname());
            stm.setString(3, user.getGender());
            stm.setString(4, user.getEmail());
            stm.setString(5, user.getMobile());
            stm.setString(6, user.getAddress());
            stm.setInt(7, user.getRoles().getRoleid());
            stm.setString(8, user.getStatus());
//            stm.setString(9, user.getImg());
            stm.setInt(9, user.getUserid());
            stm.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void register(User user) {
        try {
            connection.setAutoCommit(false);
            String sql = "INSERT INTO swp391.user \n"
                    + "(user_name,full_name, password ,gender, email, \n"
                    + "mobile, address, role_id, status, img_url)\n"
                    + "VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, user.getUsername());

            stm.setString(2, user.getFullname());
            stm.setString(3, user.getPassword());
            stm.setString(4, user.getGender());
            stm.setString(5, user.getEmail());
            stm.setString(6, user.getMobile());
            stm.setString(7, user.getAddress());
            stm.setInt(8, user.getRoles().getRoleid());
            stm.setString(9, user.getStatus());
            stm.setString(10, user.getImg());
            stm.executeUpdate();

            String sql_get_userid = "SELECT @@IDENTITY AS user_id";
            PreparedStatement stm_get_userid = connection.prepareStatement(sql_get_userid);
            ResultSet rs = stm_get_userid.executeQuery();
            if (rs.next()) {
                user.setUserid(rs.getInt("user_id"));
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void changePassword(int userId, String password) {
        try {
            connection.setAutoCommit(false);
            String sql = "UPDATE swp391.user \n"
                    + "SET password = ? "
                    + "WHERE user_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, password);
            stm.setInt(2, userId);
            stm.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
     * HuyenPTN - 25/6/2023
     * Add a new user information
     */
    public void create(User user) {
        try {
            connection.setAutoCommit(false);
            String sql = "INSERT INTO swp391.user \n"
                    + "(full_name, password ,gender, email, \n"
                    + "mobile, address, role_id, status, img_url)\n"
                    + "VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
//            stm.setString(1, user.getUsername());
//            stm.setString(2, user.getPassword());
            stm.setString(1, user.getFullname());
            stm.setString(2, user.getPassword());
            stm.setString(3, user.getGender());
            stm.setString(4, user.getEmail());
            stm.setString(5, user.getMobile());
            stm.setString(6, user.getAddress());
            stm.setInt(7, user.getRoles().getRoleid());
            stm.setString(8, user.getStatus());
            stm.setString(9, user.getImg());
            stm.executeUpdate();

            String sql_get_userid = "SELECT @@IDENTITY AS user_id";
            PreparedStatement stm_get_userid = connection.prepareStatement(sql_get_userid);
            ResultSet rs = stm_get_userid.executeQuery();
            if (rs.next()) {
                user.setUserid(rs.getInt("user_id"));
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by fullname/email/mobile
     */
    public ArrayList<User> searchByText(String searched) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE u.full_name LIKE ?\n"
                    + "OR u.email LIKE ?\n"
                    + "OR u.mobile LIKE ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
//            stm.setString(4, gender);
//            stm.setString(5, rolename);
//            stm.setString(6, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by gender
     */
    public ArrayList<User> searchByGender(String gender) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE u.gender = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, gender);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by rolename
     */
    public ArrayList<User> searchByRolename(String rolename) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE r.role_name = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, rolename);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by status
     */
    public ArrayList<User> searchByStatus(String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE u.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by fullname/email/mobile, gender, rolename and status
     */
    public ArrayList<User> searchUser1(String searched, String gender, String rolename, String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE (u.full_name LIKE ?\n"
                    + "OR u.email LIKE ?\n"
                    + "OR u.mobile LIKE ?)\n"
                    + "AND u.gender = ?\n"
                    + "AND r.role_name = ?\n"
                    + "AND u.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
            stm.setString(4, gender);
            stm.setString(5, rolename);
            stm.setString(6, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by fullnam/email/mobile and gender
     */
    public ArrayList<User> searchUser2(String searched, String gender) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE (u.full_name LIKE ?\n"
                    + "OR u.email LIKE ?\n"
                    + "OR u.mobile LIKE ?)\n"
                    + "AND u.gender = ?\n";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
            stm.setString(4, gender);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by fullnam/email/mobile and rolename
     */
    public ArrayList<User> searchUser3(String searched, String rolename) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE (u.full_name LIKE ?\n"
                    + "OR u.email LIKE ?\n"
                    + "OR u.mobile LIKE ?)\n"
                    + "AND r.role_name = ?\n";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
            stm.setString(4, rolename);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by fullnam/email/mobile and status
     */
    public ArrayList<User> searchUser4(String searched, String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE (u.full_name LIKE ?\n"
                    + "OR u.email LIKE ?\n"
                    + "OR u.mobile LIKE ?)\n"
                    + "AND u.status = ?\n";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
            stm.setString(4, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by fullnam/email/mobile, gender and rolename
     */
    public ArrayList<User> searchUser5(String searched, String gender, String rolename) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE (u.full_name LIKE ?\n"
                    + "OR u.email LIKE ?\n"
                    + "OR u.mobile LIKE ?)\n"
                    + "AND u.gender = ?\n"
                    + "AND r.role_name = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
            stm.setString(4, gender);
            stm.setString(5, rolename);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by fullnam/email/mobile, gender and status
     */
    public ArrayList<User> searchUser6(String searched, String gender, String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE (u.full_name LIKE ?\n"
                    + "OR u.email LIKE ?\n"
                    + "OR u.mobile LIKE ?)\n"
                    + "AND u.gender = ?\n"
                    + "AND u.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
            stm.setString(4, gender);
            stm.setString(5, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by fullnam/email/mobile, rolename and status
     */
    public ArrayList<User> searchUser7(String searched, String rolename, String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE (u.full_name LIKE ?\n"
                    + "OR u.email LIKE ?\n"
                    + "OR u.mobile LIKE ?)\n"
                    + "AND r.role_name = ?\n"
                    + "AND u.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + searched + "%");
            stm.setString(2, "%" + searched + "%");
            stm.setString(3, "%" + searched + "%");
            stm.setString(4, rolename);
            stm.setString(5, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by gender and rolename
     */
    public ArrayList<User> searchUser8(String gender, String rolename) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE u.gender = ?\n"
                    + "AND r.role_name = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, gender);
            stm.setString(2, rolename);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by gender and status
     */
    public ArrayList<User> searchUser9(String gender, String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE u.gender = ?\n"
                    + "AND u.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, gender);
            stm.setString(2, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by gender, rolename and status
     */
    public ArrayList<User> searchUser10(String gender, String rolename, String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE u.gender = ?\n"
                    + "AND r.role_Name = ?\n"
                    + "AND u.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, gender);
            stm.setString(2, rolename);
            stm.setString(3, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * Search users by rolename and status
     */
    public ArrayList<User> searchUser11(String rolename, String status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.user_name, u.password,\n"
                    + "u.full_name, u.gender, u.email, u.mobile, \n"
                    + "u.address, u.role_id, r.role_name, u.status\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id\n"
                    + "WHERE r.role_name = ?\n"
                    + "AND u.status = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, rolename);
            stm.setString(2, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("full_name"));
                u.setGender(rs.getString("gender"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getString("status"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setRolename(rs.getString("role_name"));
                u.getRole().add(r);

                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /*
     * HuyenPTN - 25/6/2023
     * List all author (the users with roleid = 2 (Marketing))
     */
    public ArrayList<User> listAuthor() {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "SELECT u.user_id, u.full_name, u.role_id\n"
                    + "FROM swp391.user u INNER JOIN swp391.role r\n"
                    + "ON u.role_id = r.role_id WHERE u.role_id = 2";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setFullname(rs.getString("full_name"));

                Role r = new Role();
                r.setRoleid(rs.getInt("role_id"));
                r.setUser(u);

                u.getRole().add(r);
                user.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    public User getEmail(String email) {
        try {
            String sql = "SELECT email FROM swp391.user WHERE email = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            User u = null;
            while (rs.next()) {
                if (u == null) {
                    u = new User();
                    u.setEmail(rs.getString("email"));
                }
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
