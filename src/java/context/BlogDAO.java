/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Blog;

/**
 *
 * @author BUI QUOC BAO
 */
public class BlogDAO extends DBContext{
    public ArrayList<Blog> pagging(int index){
        ArrayList<Blog> b = new ArrayList<>();
        try {
            String sql = "select * from `swp391`.`blog` order by blog_id limit 4 offset ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, (index-1)*4);
            ResultSet rs = stm.executeQuery();     
            while (rs.next()) {                
                b.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getDate(8),rs.getString(9)
                        ,rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return b;
    }
    
    public  int count(){
        try {
            String sql = "select count (*) from `swp391`.`blog`";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }
    
    public ArrayList<Blog> detail(int blog_id){
        ArrayList<Blog> b = new ArrayList<>();
        try {
            String sql = "select * from `swp391`.`blog` where blog_id = ? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, blog_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                b.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getDate(8),rs.getString(9)
                        , rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return b;
    }
    
    public ArrayList<Blog> list(){
        ArrayList<Blog> b = new ArrayList<>();
        try {
            String sql = "select * from `swp391`.`blog`";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                b.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getDate(8),rs.getString(9)
                        , rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return b;
    }
    
    public ArrayList<Blog> getbycate(int category_id){
        ArrayList<Blog> b = new ArrayList<>();
        String sql = "select * from `swp391`.`blog` where category_id = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, category_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                b.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getDate(8),rs.getString(9)
                        , rs.getString(10)));
            }
        } catch (Exception e) {
        }
                return b;
    }

}
