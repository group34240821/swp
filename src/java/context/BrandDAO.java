/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package context;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Brand;

/**
 *
 * @author BUI QUOC BAO
 */
public class BrandDAO extends DBContext{
    ArrayList<Brand> brand = new ArrayList<>();
    
    public ArrayList<Brand> list(){
        try {
            String sql = "select * from swp391.brand";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                Brand b = new Brand();
                b.setBrand_id((rs.getInt("brand_id")));
                b.setBrand_name((rs.getString("brand_name")));
                brand.add(b);
            }
        } catch (Exception e) {
        }
        return brand;
    }
}
