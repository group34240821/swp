/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author Cao Gia Linh
 */
public class Product2 {

    private int product_id;
    private String product_name;
    private double price;
    private double sale;
    private String description;
    private String pimg_url;
    private int brand_id;
    private int category_id;
    private Date createdDate;
    private int createdBy;
    private Date modifiedDate;
    private int modifieBy;
    private boolean isDelete;
    private String status;
    private Cart cart;

    public Product2() {
    }

    public Product2(int product_id, String product_name, double price, double sale, String description, String pimg_url, int brand_id, int category_id, Date createdDate, int createdBy, Date modifiedDate, int modifieBy, boolean isDelete, String status) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.price = price;
        this.sale = sale;
        this.description = description;
        this.pimg_url = pimg_url;
        this.brand_id = brand_id;
        this.category_id = category_id;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.modifiedDate = modifiedDate;
        this.modifieBy = modifieBy;
        this.isDelete = isDelete;
        this.status = status;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSale() {
        return sale;
    }

    public void setSale(double sale) {
        this.sale = sale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPimg_url() {
        return pimg_url;
    }

    public void setPimg_url(String pimg_url) {
        this.pimg_url = pimg_url;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public int getModifieBy() {
        return modifieBy;
    }

    public void setModifieBy(int modifieBy) {
        this.modifieBy = modifieBy;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    @Override
    public String toString() {
        return "product{" + "product_id=" + product_id + ", product_name=" + product_name + ", price=" + price + ", sale=" + sale + ", description=" + description + ", pimg_url=" + pimg_url + ", brand_id=" + brand_id + ", category_id=" + category_id + ", createdDate=" + createdDate + ", createdBy=" + createdBy + ", modifiedDate=" + modifiedDate + ", modifieBy=" + modifieBy + ", isDelete=" + isDelete + ", status=" + status + '}';
    }

}
