/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author BUI QUOC BAO
 */
public class Blog {
    
    int blog_id;
    String thumbnail_url;
    String title;
    int category_id;
    int author;
    String summary;
    String content;
    Date update_date;
    String status;
    String flag;

    public Blog() {
    }

    public Blog(int blog_id, String thumbnail_url, String title, int category_id, int author, String summary, String content, Date update_date, String status, String flag) {
        this.blog_id = blog_id;
        this.thumbnail_url = thumbnail_url;
        this.title = title;
        this.category_id = category_id;
        this.author = author;
        this.summary = summary;
        this.content = content;
        this.update_date = update_date;
        this.status = status;
        this.flag = flag;
    }

    public int getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(int blog_id) {
        this.blog_id = blog_id;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Blog{" + "blog_id=" + blog_id + ", thumbnail_url=" + thumbnail_url + ", title=" + title + ", category_id=" + category_id + ", author=" + author + ", summary=" + summary + ", content=" + content + ", update_date=" + update_date + ", status=" + status + ", flag=" + flag + '}';
    }
}
