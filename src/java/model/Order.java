/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author BUI QUOC BAO
 */
public class Order {
    private int order_id;
    private int user_id;
    private String status;
    private Date createdDate;
    private Double total_price;

    public Order() {
    }

    public Order(int order_id, int user_id, String status, Date createdDate, Double total_price) {
        this.order_id = order_id;
        this.user_id = user_id;
        this.status = status;
        this.createdDate = createdDate;
        this.total_price = total_price;
    }
    
    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreateDate(Date createDate) {
        this.createdDate = createDate;
    }

    public Double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(Double total_price) {
        this.total_price = total_price;
    }

    @Override
    public String toString() {
        return "Order{" + "order_id=" + order_id + ", user_id=" + user_id + ", status=" + status + ", createdDate=" + createdDate + ", total_price=" + total_price + '}';
    }
    
    
}
