/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Mạc Huyền
 */
public class Cart {
    private int cartid;
    private int quantity;
    ArrayList<Product2> product = new ArrayList<>();
    ArrayList<User> user = new ArrayList<>();
    
    public double getTotal()
    {
        double sum = 0;
        for (Product2 products : product) {
            sum += quantity * products.getPrice();
        }
        return sum;
    }

    public int getCartid() {
        return cartid;
    }

    public void setCartid(int cartid) {
        this.cartid = cartid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public ArrayList<Product2> getProduct() {
        return product;
    }

    public void setProduct(ArrayList<Product2> product) {
        this.product = product;
    }

    public ArrayList<User> getUser() {
        return user;
    }

    public void setUser(ArrayList<User> user) {
        this.user = user;
    }
    
}
