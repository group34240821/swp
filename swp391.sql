CREATE DATABASE  IF NOT EXISTS `swp391` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `swp391`;
-- MySQL dump 10.13  Distrib 8.0.33, for Win64 (x86_64)
--
-- Host: localhost    Database: swp391
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog` (
  `blog_id` int NOT NULL AUTO_INCREMENT,
  `thumbnail_url` varchar(255) DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `category_id` int NOT NULL,
  `author` int DEFAULT NULL,
  `summary` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `content` longtext NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(50) NOT NULL,
  `flag` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`blog_id`),
  KEY `category_id_idx` (`category_id`),
  KEY `author` (`author`),
  CONSTRAINT `author` FOREIGN KEY (`author`) REFERENCES `user` (`user_id`),
  CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'portableSSDs-2048px-4747-2x1-1.webp','The Best Portable SSD',1,2,'If you need a fast, reliable way to move files between computers, the Samsung T7 Shield (1 TB) is the portable solid-state drive for you.','post1-detail.jsp','2023-06-20 00:00:00','Published','on'),(2,'androidtablets-2048px-allflat-2x1-1.webp','The Best Android Tablets',5,9,'Good Android tablets are few and far between, but if you don’t want an iPad, Samsung’s Galaxy Tab S8 is your best bet.','post2-detail.jsp','2023-06-20 01:00:00','Published','on'),(3,'businesslaptops-2048px-0098-2x1-1.webp','The Best Business Laptops',3,2,'An ultrabook is likely to be the best laptop for most people, but business laptops hold up to travel and frequent use thanks to their repairability.','post3-detail.jsp','2023-06-25 00:05:57','Published','on'),(4,'4kmonitors-2048px-9869-2x1-1.webp','The Best 4K Monitors',2,9,'If you’re a video-editing pro or love to watch high-res movies, the best 4K monitor is the Dell UltraSharp U2723QE.','post4-detail.jsp','2023-06-25 00:18:32','Future','off'),(5,'androidphones-2048px-2x1-1.webp','The 5 Best Android Phones',4,9,'For just about everyone but the most demanding phone photographers, Google’s Pixel 7 phones are the best Android phones available.','post5-detail.jsp','2023-06-25 19:52:59','Pending','off'),(6,'laptopsforcollegestudents-2048px-0987-2x1-1.webp','The Best Laptops for College Students',3,2,'Of all the laptops we recommend, these offer the best balance of performance and a low price—and that makes them our favorite laptops for college students.','post6-detail.jsp','2023-06-25 19:54:41','Draft','on'),(7,'workoutheadphones-2048px-2358-2x1-1.webp','The Best Workout Headphones',6,2,'The JBL Reflect Aero TWS are our favorite earbuds for the gym. We love their secure fit, simple controls, waterproof design, and ability to block noise.','<p>Few things will kill your workout vibe faster than a pair of ill-fitting, hard-to-use headphones. The completely wireless&nbsp;<strong><a href=\"https://www.nytimes.com/wirecutter/out/link/52161/183696/4/151630/?merchant=Amazon\" target=\"_blank\">JBL Reflect Aero TWS</a></strong>&nbsp;set is our favorite for the gym. We love their secure fit, simple controls, waterproof design, and ability to block noise. Plus, these earbuds sound great.</p>\r\n\r\n<p>The&nbsp;<strong><a href=\"https://www.nytimes.com/wirecutter/out/link/52161/183696/4/151630/?merchant=Amazon\" target=\"_blank\">JBL Reflect Aero TWS</a></strong>&nbsp;true wireless earbuds are the best workout headphones for the gym because they stay securely in place, they have simple controls, and they&rsquo;re waterproof and sweatproof (with an&nbsp;<strong><a href=\"https://www.nytimes.com/wirecutter/blog/how-much-water-will-wreck-your-gadgets/\">IP68</a></strong>&nbsp;water-resistance rating). Eight hours of battery life is solid for true wireless earbuds, and the pocket-size case offers up an additional 16 hours of juice.</p>\r\n\r\n<p>This pair also sounds good right out of the box, and you can fine-tune the sound via JBL&rsquo;s mobile app. The sealed design and active noise cancellation can reduce any noisy gym distractions around you. But if you need to hear your surroundings, a hear-through mode is just a tap away. Or, if you prefer, you can use one earbud at a time. Alexa and Google Assistant users will love this set&rsquo;s compatibility with their voice-activated assistants.</p>\r\n\r\n<p>As for downsides, the stabilizing wings that help keep the earbuds in place may become fatiguing for people with sensitive ears. And though the touch-based controls are easy to learn and use, you can&rsquo;t control volume, playback, noise cancellation, and the hear-through feature inclusively&mdash;you&rsquo;ll have to choose one to omit.</p>\r\n\r\n<p>For Apple fans, the&nbsp;<strong><a href=\"https://www.nytimes.com/wirecutter/out/link/47830/176711/4/137819/?merchant=Amazon\" target=\"_blank\">Beats Fit Pro</a></strong>&nbsp;pair is a worthy alternative to the JBL Reflect Aero TWS. This set offers easy pairing and connection swapping to Apple devices, as well as touch-free &ldquo;Hey Siri&rdquo; control and physical buttons that can control track skip, phone calls, and volume on Apple devices (plus some button customization for Android devices, if you download the Beats app).</p>\r\n\r\n<p>Like our top pick, these earbuds have stabilizing wings to help keep them in place, and the sealed design and active noise cancellation can block out gym noise; the hear-through mode allows for occasional situational awareness or conversations between workout sets. Plus, each earbud can work independently.</p>\r\n\r\n<p>The IPX4 water-resistance rating should be enough for these earbuds to tolerate most activities, but our top pick&rsquo;s IP68 rating is better for folks who sweat profusely or are just tougher on their gear.</p>\r\n\r\n<p>The Fit Pro pair sounds great, with slightly boosted bass. Six hours of battery life is average, and you get 18 more hours from the charging case&mdash;but the case isn&rsquo;t as small as the JBL case.</p>\r\n\r\n<p>As is the case with our top pick, those who are sensitive to pressure in the ear might dislike the feeling of the stabilizing wings.</p>\r\n\r\n<p>The&nbsp;<strong><a href=\"https://www.nytimes.com/wirecutter/out/link/52727/187121/4/151632/?merchant=Amazon\" target=\"_blank\">JLab Go Air Sport</a></strong>&nbsp;earbuds deliver the workout essentials without costing a bundle. This pair has flexible hooks that slip securely over the top of each ear to keep the earbuds in place; some people may find these hooks more comfortable than stabilizing wings.</p>\r\n\r\n<p>This set is dust- and water-resistant, with an&nbsp;<strong><a href=\"https://www.nytimes.com/wirecutter/blog/how-much-water-will-wreck-your-gadgets/\">IP55</a></strong>&nbsp;rating to endure sweaty workouts. The earbuds have an eight-hour battery life, and the charging case holds an additional 24 hours of power (though it&rsquo;s larger than we&rsquo;d like).</p>\r\n\r\n<p>The Go Air Sport has a sealed design to block noise, but it lacks active noise cancellation and a hear-through mode. However, either earbud can work independently, so you can keep one ear open. The touch-based controls handle all the important functions, but their response can be inconsistent, especially if you have long hair, which can get in the way of the sensors.</p>\r\n\r\n<p>The sound quality is surprisingly decent for the price, but audio fans should note that every one of the three preset EQ settings has some kind of flaw. We like that the charging cable is permanently attached, so you never need to carry a cord. But the cable ends in a USB Type-A plug, a style that is falling out of fashion.</p>\r\n\r\n<p>If you want to worry less about your earbuds&rsquo; battery life, the&nbsp;<strong><a href=\"https://www.nytimes.com/wirecutter/out/link/52728/184882/4/151633/?merchant=Amazon\" target=\"_blank\">Tribit MoveBuds H1</a></strong>&nbsp;set is a solid pair of true wireless earbuds usually available for under $100. The 15-hour battery life is superlative for true wireless earbuds; you can wear them all day without charging. This is a good thing because the charging case&mdash;which provides an additional 35 hours of power&mdash;is nearly as big as a bar of soap.</p>\r\n\r\n<p>This waterproof pair has a high&nbsp;<strong><a href=\"https://www.nytimes.com/wirecutter/blog/how-much-water-will-wreck-your-gadgets/\">IPX8</a></strong>&nbsp;rating, and the earbuds stay securely in place using hooks that slip over your ears. The sealed design helps block out gym noise, but the H1 lacks the active noise cancellation and voice-activation capabilities of our pricier picks. This set does have a hear-through mode, or you can use one earbud at a time for situational awareness.</p>\r\n\r\n<p>The sound quality out of the box is good, and we were able to nail our preferred tuning using the app-based equalizer adjustments. The H1 set also has the full suite of touch-based controls.</p>\r\n','2023-06-26 00:48:13','Pending',NULL),(19,'gamingconsoles-2048px-00730-2048px-2x1-1.webp','Choosing the Right Xbox: Series X or Series S',7,2,'In 2023, fewer games are coming to last-generation consoles. If you’re looking for a new Xbox, we’re here to help you pick the right one.','<p>The current generation of Xbox consoles presents what might seem like a confusing choice: the $500&nbsp;<a href=\"https://www.nytimes.com/wirecutter/out/link/41519/166570/4/122583/?merchant=Amazon\" rel=\"sponsored noopener\" target=\"_blank\">Xbox Series X</a>&nbsp;and the $300&nbsp;<a href=\"https://www.nytimes.com/wirecutter/out/link/41520/167086/4/122584/?merchant=Amazon\" rel=\"sponsored noopener\" target=\"_blank\">Xbox Series S</a>. The two are based on a similar foundation, and they will play all of the same games for years to come. But the Series X and Series S each target a different level of graphics performance, and they take different approaches to physical media, as the Series X supports discs while the Series S is digital only. With that in mind, we&rsquo;re here to help you figure out which Xbox you should buy&mdash;or if you need to buy one at all.</p>\r\n\r\n<p>With better graphics, more storage, and a disc drive, the&nbsp;<a href=\"https://www.nytimes.com/wirecutter/out/link/41519/166570/4/122583/?merchant=Amazon\" rel=\"sponsored noopener\" target=\"_blank\">Microsoft Xbox Series X</a>&nbsp;justifies its higher price&mdash;but whether those features are worth paying for depends on what kind of display you&rsquo;re playing it on. If you have a 4K TV with high-end features such as Dolby Vision, 120 Hz, and variable refresh rate, or if you plan to get one, the Series X is likely worth choosing over the Series S. Both consoles offer impressively fast storage that improves boot-up and load times, but the Series X has more than 800 GB of storage available internally, nearly twice the available capacity of the Series S. Plus, if you have a collection of physical Xbox One, Xbox 360, or original-Xbox games that you want to keep playing, only the Series X has a disc drive, which also makes it the model of choice if you want a console that can also be your 4K Blu-ray player.</p>\r\n\r\n<p>On the other hand, if you don&rsquo;t have a 4K TV (and won&rsquo;t buy one soon), if space in your entertainment center is a concern, or if you don&rsquo;t have or plan to buy many physical discs, the&nbsp;<a href=\"https://www.nytimes.com/wirecutter/out/link/41520/167086/4/122584/?merchant=Amazon\" rel=\"sponsored noopener\" target=\"_blank\">Microsoft Xbox Series S</a>&nbsp;offers a lot of&nbsp;<a href=\"https://www.nytimes.com/wirecutter/blog/xbox-series-s-black-friday-great-alternative-to-ps5-xbox-series-x/\">value</a>&nbsp;and still gives you the chance to play the new generation of games to come. And anyone who mostly plays games from&nbsp;<a href=\"https://www.nytimes.com/wirecutter/out/link/25268/134341/4/123428/?merchant=Microsoft\" rel=\"sponsored noopener\" target=\"_blank\">Xbox Game Pass</a>&mdash;the monthly subscription service that provides access to hundreds of games, including new releases&mdash;probably won&rsquo;t miss the disc drive. We also think that makes the Series S an especially budget-conscious choice for younger kids, since the less expensive console and monthly membership add up to more games than a kid can play, so you have no need to buy new ones all the time. The Series S might be a good option for some travelers and hosts, too: If you&rsquo;re going somewhere for an extended stay, the Series S can easily fit in a carry-on piece of luggage, and if you have a guest room or vacation rental, a Series S adds a lot beyond a regular media streaming box.</p>\r\n','2023-07-09 00:15:07','Published','off');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_topic`
--

DROP TABLE IF EXISTS `blog_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog_topic` (
  `blog_id` int DEFAULT NULL,
  `topic_id` int DEFAULT NULL,
  KEY `blog_id` (`blog_id`),
  KEY `topic_id` (`topic_id`),
  CONSTRAINT `blog_topic_ibfk_1` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`blog_id`),
  CONSTRAINT `blog_topic_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_topic`
--

LOCK TABLES `blog_topic` WRITE;
/*!40000 ALTER TABLE `blog_topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brand` (
  `brand_id` int NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` VALUES (1,'AAAA');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Storage devices'),(2,'Computers'),(3,'Laptops'),(4,'Smartphones'),(5,'Tablets'),(6,'Headphones'),(7,'Gaming'),(8,'Cameras'),(9,'Audio'),(10,'Batteries and charging');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_changes`
--

DROP TABLE IF EXISTS `customer_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_changes` (
  `change_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `full_name` varchar(100) NOT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `update_by` int DEFAULT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`change_id`),
  KEY `update_by_idx` (`update_by`),
  CONSTRAINT `update_by` FOREIGN KEY (`update_by`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_changes`
--

LOCK TABLES `customer_changes` WRITE;
/*!40000 ALTER TABLE `customer_changes` DISABLE KEYS */;
INSERT INTO `customer_changes` VALUES (43,116,'Quỳnh Anh',NULL,'kelma.b.alone@gmail.com','9876543210','Hải Phòng','Active',NULL,'2023-07-07 01:43:57'),(44,117,'Hạ Du Kiệt',NULL,'fushigurohouseki@gmail.com','0987654321','','Contact',NULL,'2023-07-07 01:48:55'),(45,117,'Hạ Du Kiệt','','fushigurohouseki@gmail.com','0987654321','','Potential',NULL,'2023-07-07 01:49:27'),(46,118,'Roland Eldric',NULL,'rolandeldric@gmail.com','1234509876','','Customer',NULL,'2023-07-07 01:50:30'),(47,122,'Mạc Huyền',NULL,'machuyen182769@gmail.com','','','Active',NULL,'2023-07-08 02:44:09'),(48,123,'Mạc Huyền',NULL,'machuyen182769@gmail.com','','','Active',NULL,'2023-07-08 02:45:42'),(49,155,'Bạch Phong',NULL,'bachtieudien@gmail.com','1122334455','','Contact',NULL,'2023-07-08 15:40:03'),(50,158,'Lilia Vongola','Female','liliavongola@gmail.com','0377904871','Italia','Contact',NULL,'2023-07-08 17:41:31'),(51,158,'Lilia Vongola','Female','liliavongola@gmail.com','0377904871','Italia','Potential',NULL,'2023-07-08 22:42:30'),(52,158,'Lilia Vongola','Female','liliavongola@gmail.com','0377904871','Italia','Customer',NULL,'2023-07-08 22:43:31'),(53,160,'Thanh','Female','sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 22:46:38'),(54,161,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 22:48:21'),(55,162,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 22:50:03'),(56,163,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 22:51:11'),(57,164,'Bạch Phong',NULL,'bachtieudien@gmail.com','0366875345','','Potential',NULL,'2023-07-08 22:53:29'),(58,165,'Mạc Huyền',NULL,'machuyen182769@gmail.com','0366875345','','Customer',NULL,'2023-07-08 22:54:54'),(59,166,'Bạch Phong',NULL,'bachtieudien@gmail.com','0512345678','','Customer',NULL,'2023-07-08 22:57:57'),(60,167,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Active',NULL,'2023-07-08 22:59:23'),(61,168,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:00:40'),(62,169,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:04:29'),(63,170,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:06:07'),(64,171,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:08:35'),(65,172,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:09:40'),(66,173,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:12:25'),(67,174,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:13:46'),(68,175,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:14:58'),(69,176,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:16:57'),(70,177,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:19:36'),(71,178,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:43:53'),(72,180,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-08 23:58:59'),(73,181,'Thanh',NULL,'sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-09 00:04:52'),(74,181,'Thanh','','sawadahoseki@gmail.com','0355235054','','Potential',NULL,'2023-07-09 00:06:26'),(75,181,'Thanh','','sawadahoseki@gmail.com','0355235054','','Customer',NULL,'2023-07-09 00:06:39');
/*!40000 ALTER TABLE `customer_changes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback` (
  `feedback_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `rate` tinyint DEFAULT NULL,
  `comment` longtext,
  PRIMARY KEY (`feedback_id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `total_price` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,2,'accpect','2023-06-12 00:00:00',1000),(2,3,'watting','2023-06-14 00:00:00',5000),(3,4,'watting','2023-06-17 00:00:00',100000),(4,5,'not accpect','2023-06-18 00:00:00',120),(5,6,'accpect','2023-06-19 00:00:00',2000),(6,7,'accpect','2023-06-20 00:00:00',12000);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_detail` (
  `order_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  KEY `product_id` (`product_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `order_detail_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `order_detail_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `sale` decimal(10,0) DEFAULT NULL,
  `description` longtext,
  `pimg_url` varchar(255) DEFAULT NULL,
  `brand_id` int DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `createdDate` date DEFAULT NULL,
  `createdBy` int DEFAULT NULL,
  `modifiedDate` date DEFAULT NULL,
  `modifiedBy` int DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `createdBy` (`createdBy`),
  KEY `modifiedBy` (`modifiedBy`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `user` (`user_id`),
  CONSTRAINT `product_ibfk_2` FOREIGN KEY (`modifiedBy`) REFERENCES `user` (`user_id`),
  CONSTRAINT `product_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`),
  CONSTRAINT `product_ibfk_4` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'ABC',200,100,'ABC','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',1,'2023-06-12',1,_binary '\0','ok'),(2,'CDE',300,50,'CDE','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',1,'2023-06-12',1,_binary '\0','ok'),(4,'test2',100,10,'test1','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',NULL,'2023-06-12',NULL,NULL,'ok'),(5,'test3',100,10,'test3','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',NULL,'2023-06-12',NULL,NULL,'ok'),(6,'test4 ',100,10,'test4','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',NULL,'2023-06-12',NULL,NULL,''),(7,'test5',100,10,'test5','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',NULL,'2023-06-12',NULL,NULL,'ok'),(9,'iphone7',500000,0,'khong','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',1,'2023-06-12',1,_binary '\0','Active'),(10,'iphone8',500000,0,'khong','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',1,'2023-06-12',1,_binary '\0','Active'),(11,'iphone9',500000,0,'khong','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',1,'2023-06-12',1,_binary '\0','Active'),(12,'iphone10',500000,0,'khong','https://i.imgur.com/bCpj2ZA.jpg',1,1,'2023-06-12',1,'2023-06-12',1,_binary '\0','Active');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin'),(2,'Marketing'),(3,'Sale'),(4,'Customer');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slider` (
  `slider_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `note` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `topic` (
  `topic_id` int NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `role_id` int NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Active',
  `img_url` longtext,
  PRIMARY KEY (`user_id`),
  KEY `role_id_idx` (`role_id`),
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','111','Admin','Male','amduongdien182769@gmail.com','','',1,'Active',NULL),(2,'marketing','111','Marketing','','marketing@gmail.com','','',2,'Active','avatar-default-symbolic.png'),(3,'sale','111','Sale','','sale@gmail.com','','',3,'Active',NULL),(4,'huyenptn','123','Phạm Ngọc Huyền','Female','huyenptn182769@gmail.com','','',4,'Active','HE160769.png'),(5,'thanhpn','123','Phùng Ngọc Thanh','Female','thanhpn@gmail.com','','HP',4,'Potential','HE162222.png'),(6,'linhcg','123','Cao Gia Linh','Male','linhcg@gmail.com',NULL,NULL,4,'Active',NULL),(7,'bachpq','123','Phạm Quý Bách','Male','bachpq@gmail.com',NULL,NULL,4,'Active',NULL),(8,'longdh','123','Đỗ Hải Long','Male','longdh@gmail.com','0123456789','',3,'Active','HE167777.png'),(9,'hoanglh','123','Lý Hải Hoàng','Male','hoanglh@gmail.com','','',2,'Inactive','HE166666.png'),(10,'anhdn','123','Đỗ Ngọc Anh','Female','anhdn@gmail.com','','',4,'Potential',NULL),(116,NULL,'iXKc@OI','Quỳnh Anh',NULL,'kelma.b.alone@gmail.com','9876543210','Hải Phòng',4,'Active','HE164444.png'),(117,NULL,'LW1Aj&&','Hạ Du Kiệt','','fushigurohouseki@gmail.com','0987654321','',4,'Potential','HE165555.png'),(118,NULL,'SyH(xPv','Roland Eldric',NULL,'rolandeldric@gmail.com','1234509876','',4,'Customer',NULL),(158,NULL,'PUnkdCB','Lilia Vongola','Female','liliavongola@gmail.com','0377904871','Italia',4,'Customer','HE163333.png'),(166,NULL,'IXE3FU9','Bạch Phong',NULL,'bachtieudien@gmail.com','0512345678','',4,'Customer',NULL),(179,NULL,'Q)Jes6g','Mạc Huyền',NULL,'machuyen182769@gmail.com','','',3,'Active','avatar-default-symbolic.png'),(181,NULL,'bc%D$%!','Thanh','','sawadahoseki@gmail.com','0355235054','',4,'Customer','HE165555.png'),(182,NULL,'V!iweH)','ABC',NULL,'nkcuisde@gmail.com','','',3,'Active','70837564_p1.jpg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'swp391'
--

--
-- Dumping routines for database 'swp391'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-20 13:08:17
