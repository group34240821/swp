<%-- 
    Document   : menu.jsp
    Created on : Jun 13, 2023, 12:02:16 AM
    Author     : DW
--%>

<!-- Start Mobile Menu Area -->
<div class="mobile-menu-area">

    <!--offcanvas menu area start-->
    <div class="off_canvars_overlay">

    </div>
    <div class="offcanvas_menu">
        <div class="offcanvas_menu_wrapper">
            <div class="canvas_close">
                <a href="javascript:void(0)"><i class="fas fa-times"></i></a>  
            </div>
            <div class="mobile-logo">
                <h2><a href="index.html"><img src="assets/img/logo.png"></a></h2>
            </div>
            <div id="menu" class="text-left ">
                <ul class="offcanvas_main_menu">
                    <li class="menu-item-has-children">
                        <a href="index.html">Home</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="about.html">about Us</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Page</a>
                        <ul class="sub-menu">
                            <li><a href="cart.html">Cart</a></li>
                            <li><a href="wishlist.html"> Wishlist</a></li>
                            <li><a href="checkout.html">Checkout</a></li>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="register.html">Register</a></li>
                            <li><a href="reset-password.html">Reset Password</a></li>
                            <li><a href="privacy-policy.html">Privacy Policy</a></li>
                            <li><a href="terms-condition.html">Terms & Condition</a></li>
                            <li><a href="404.html">404 Error</a></li>
                            <li><a href="faq.html">Faq</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Shop</a>
                        <ul class="sub-menu">
                            <li><a href="shop.html">Shop</a></li>
                            <li><a href="shop2-columns.html">Shop 2 Columns</a></li>
                            <li><a href="shop-grid.html">Shop Grid</a></li>
                            <li><a href="shop-left-sidebar.html">Shop Left Sidebar</a></li>
                            <li><a href="shop-list.html">Shop List</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Elements</a>
                        <ul class="sub-menu">
                            <li class="menu-item-has-children">
                                <a href="#">Elements</a>
                                <ul class="sub-menu">
                                    <li><a href="element-infobox.html">Element Info Box</a></li>
                                    <li><a href="element-breadcrumb.html">Element Breadcrum</a></li>
                                    <li><a href="element-heading.html">Element Headding</a></li>
                                    <li><a href="element-post.html">Element Post Element</a></li>
                                    <li><a href="element-pricing.html">Element Pricing</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Elements</a>
                                <ul class="sub-menu">
                                    <li><a href="element-product-category.html">Element Product Category</a></li>
                                    <li><a href="element-product-style.html">Element Product Style</a></li>
                                    <li><a href="element-product-tab.html">Element Product Tab</a></li>
                                    <li><a href="element-team-style.html">Element Team</a></li>
                                    <li><a href="element-testimonial.html">Element Testimonial</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Elements</a>
                                <ul class="sub-menu">
                                    <li><a href="shop.html">Element Shop</a></li>
                                    <li><a href="shop2-columns.html">Element Shop 2 Columns</a></li>
                                    <li><a href="shop-grid.html">Element Shop Grid</a></li>
                                    <li><a href="shop-left-sidebar.html">Element Shop Left Sidebar</a></li>
                                    <li><a href="shop-list.html">Element Shop List</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Elements</a>
                                <ul class="sub-menu">
                                    <li><a href="product-details.html">Element Shop Single</a></li>
                                    <li><a href="cart.html">Element Cart Page</a></li>
                                    <li><a href="checkout.html">Element CheckOut Page</a></li>
                                    <li><a href="wishlist.html">Element Wishlist</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Blog</a>
                        <ul class="sub-menu">
                            <li><a href="blog.html">Blog</a></li>
                            <li><a href="blog-grid.html">Blog Grid</a></li>
                            <li><a href="single.html">Blog Single</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="contact.html"> Contact Us</a> 
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--offcanvas menu area end-->
