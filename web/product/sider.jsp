<%-- 
    Document   : sider
    Created on : Jul 23, 2023, 12:59:21 AM
    Author     : Cao Gia Linh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Siderbar -->
<div class="col-lg-4">

    <!-- Search Bar -->
    <div class="sidebar-widgets">
        <!-- Can co them filter -->
        <h4 class="title">Search</h4>
        <form action="search" method="post" >
            <input  type="search" name="txtSearch" placeholder="Search Here..">
            <button type="submit"><i class="fas fa-search"></i></button>                          
        </form>
    </div>

    <div class="sidebar-widgets">
        <h4 class="title">Product Categories</h4>
        <form action="search" method="post" >
            <div class="tags">
                <c:forEach items="${requestScope.listC}" var="c">
                    <a href ="">
                        ${c.category_name}                                        
                    </a>
                </c:forEach>
            </div>
        </form>
    </div>


    <!-- Trending -->
    <div class="sidebar-widgets">
        <h4 class="title">Latest Products</h4>
        <div class="widgets-latest-product-full">
            <!-- Single -->

            <c:forEach begin="0" end="2" items="${requestScope.listPL}" var="pL">

                <div class="widgets-latest-product-single mb-30">
                    <div class="thumbanil">
                        <a href="product-detail?product_id=${pL.product_id}">
                            <img src="./assets/img/product/${pL.pimg_url}" alt="Products">
                        </a>
                    </div>
                    <div class="content">
                        <h4><a href="product-detail?product_id=${pL.product_id}">${pL.product_name}</a></h4>
                        <div class="pricing">
                            <span style="font-size: 23px">${pL.sale} <del>${pL.price}</span>
                        </div>
                    </div>
                </div>

            </c:forEach>
        </div>   
    </div>


    <!-- Single -->
    <div class="sidebar-widgets">
        <h4 class="title">More About Us</h4>
        <div style="font-size: 25px; font-weight:  bold ">
            Contact
            <div style="font-size: 18px; font-weight:  400 ">
                <ul >                          
                    <li>Member: <a href="">Bao</a></li>
                    <li>Member: <a href="">Bach</a></li>
                    <li>Member: <a href="">Linh</a></li>
                    <li>Member: <a href="">Hung</a></li>
                    <li>Member: <a href="">Huyen</a></li>

                </ul>
            </div>
        </div>
    </div>
</div>