<%-- 
    Document   : Feedback
    Created on : Jun 28, 2023, 9:50:43 PM
    Author     : Pc
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html  class="no-js" lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Online Shopping | Home</title>
        <link rel="icon" href="assets/img/icon.png" type="image/gif" sizes="16x16">
        <link rel="icon" href="assets/img/icon.png" type="image/gif" sizes="18x18">
        <link rel="icon" href="assets/img/icon.png" type="image/gif" sizes="20x20">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/fontawesome.all.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/responsive.css">
    </head>
    <body>
        <!-- Header -->
        <header class="header">
            <!-- Header Top -->
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="top-text">
                                <p>Welcome to Online Shop</p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="top-list">
                                <a href="#"><i class="fas fa-mobile-alt"></i> +02456 054546</a>
                                <a href="wishlist.html"><i class="far fa-heart"></i> Wishlist</a>
                                <c:if test="${sessionScope.acc == null}">
                                    <a href="login"><i class="fas fa-user"></i> Login / Register</a>
                                </c:if>
                                <c:if test="${sessionScope.acc != null}">
                                    <a href="logout"><i class="fas fa-user"></i>${sessionScope.acc.username}<i class="fa fa-angle-down"></i></a>
                                        </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header Middle -->
            <div class="header-middle pt-30 pb-30">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="col-lg-2">
                            <div class="logo">
                                <h2><a href="index.html"><img src="assets/img/logo.png"></a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header Bottom -->
            <div class="header-bottm">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="logo-2">
                                <h2><a href="index.html"><img src="assets/img/logo.png"></a></h2>
                            </div>

                            <div class="menu">
                                <nav>
                                    <ul>
                                        <li><a href="#">Home</a></li>
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="#">Page <i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu-item">
                                                <li><a href="cart.html">Cart</a></li>
                                                <li><a href="wishlist.html"> Wishlist</a></li>
                                                <li><a href="checkout.html">Checkout</a></li>
                                                <li><a href="login.html">Login</a></li>
                                                <li><a href="register.html">Register</a></li>
                                                <li><a href="reset-password.html">Reset Password</a></li>
                                                <li><a href="${pageContext.request.contextPath}/trending-product">List Trending Product</a></li>
                                                <li><a href="${pageContext.request.contextPath}/feedback">Default Feedback</a></li>
                                                <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                                <li><a href="terms-condition.html">Terms & Condition</a></li>
                                                <li><a href="404.html">404 Error</a></li>
                                                <li><a href="faq.html">Faq</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="shop.html">Shop <i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu-item">
                                                <li><a href="shop.html">Shop</a></li>
                                                <li><a href="shop2-columns.html">Shop 2 Columns</a></li>
                                                <li><a href="shop-grid.html">Shop Grid</a></li>
                                                <li><a href="shop-left-sidebar.html">Shop Left Sidebar</a></li>
                                                <li><a href="shop-list.html">Shop List</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Elements <i class="fa fa-angle-down"></i></a>
                                            <ul class="mega-sub-menu">
                                                <li>
                                                    <a class="menu-title" href="#">Element List</a>
                                                    <ul>
                                                        <li><a href="element-infobox.html">Element Info Box</a></li>
                                                        <li><a href="element-breadcrumb.html">Element Breadcrum</a></li>
                                                        <li><a href="element-heading.html">Element Headding</a></li>
                                                        <li><a href="element-post.html">Element Post Element</a></li>
                                                        <li><a href="element-pricing.html">Element Pricing</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a class="menu-title" href="#">Element</a>
                                                    <ul>
                                                        <li><a href="element-product-category.html">Element Product Category</a></li>
                                                        <li><a href="element-product-style.html">Element Product Style</a></li>
                                                        <li><a href="element-product-tab.html">Element Product Tab</a></li>
                                                        <li><a href="element-team-style.html">Element Team</a></li>
                                                        <li><a href="element-testimonial.html">Element Testimonial</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a class="menu-title" href="#">Element List</a>
                                                    <ul>
                                                        <li><a href="shop.html">Element Shop</a></li>
                                                        <li><a href="shop2-columns.html">Element Shop 2 Columns</a></li>
                                                        <li><a href="shop-grid.html">Element Shop Grid</a></li>
                                                        <li><a href="shop-left-sidebar.html">Element Shop Left Sidebar</a></li>
                                                        <li><a href="shop-list.html">Element Shop List</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a class="menu-title" href="#">Element List</a>
                                                    <ul>
                                                        <li><a href="product-details.html">Element Shop Single</a></li>
                                                        <li><a href="cart.html">Element Cart Page</a></li>
                                                        <li><a href="checkout.html">Element CheckOut Page</a></li>
                                                        <li><a href="wishlist.html">Element Wishlist</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="bloglist">blog <i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu-item">
                                                <li><a href="bloglist">Blog</a></li>
                                                <li><a href="blog-grid.html">Blog Grid</a></li>
                                                <li><a href="single.html">Blog Single</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.html">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header -->
        <!-- Start BreadCrumb Area -->
        <div class="breadcrumb-area pt-100 pb-100" style="background-image: url('assets/img/breadcrumb.jpg');">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-content">
                            <h2>Feedback</h2>
                            <ul>
                                <li><a href="index.html">Home</a></li>
                                <li class="active">Feedback Page</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End BreadCrumb Area -->

        <!-- Start Body -->
        <div class="col-12">
            <!-- Single -->
            <div class="sidebar-widgets">
                <h4 class="title">Search by full name</h4>
                <form action="FeedbackByFullName">
                    <input type="search" name="searchFullName" placeholder="Search Here..">
                    <button type="submit">Search</button>
                </form>
            </div> 
            <div class="sidebar-widgets">
                <h4 class="title">Search by content</h4>
                <form action="FeedbackByContent">
                    <input type="search" name="searchContent" placeholder="Search Here..">
                    <button type="submit">Search</button>
                </form>
            </div> 
        </div>
        <!-- Feedback -->
        <div class="col-12">
            <div class="cart-table table-responsive">
                <table class="table table-bordered align-middle text-center">
                    <thead>
                        <tr>
                            <th class="pro-title">User</th>
                            <th class="pro-thumbnail">Thumbnail</th>
                            <th class="pro-title">Product</th>
                            <th class="pro-title">Comment</th>
                            <th class="pro-title">Status</th>
                            <th class="pro-remove">Delete</th>
                        </tr>
                    </thead>
                    <tbody> 
                        <c:forEach items="${requestScope.List_Feedback}" var="item">
                            <tr>
                                <td class="pro-title" >${item.full_name} - ${item.feedback_id}</td>
                                <td class="pro-thumbnail"><img class="img-fluid" src=${item.product_img} alt="Product"></td>
                                <td class="pro-title">${item.product_name}</td>
                                <td class="pro-title">${item.comment}</td>
                                <td class="pro-title">${item.status}</td>
                                <td class="pro-remove"><a href="#"><i class="fas fa-times"></i></a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End Feedback -->
        <div class="row">
            <div class="col-12 mb-30">
                <div class="page-pagination text-center">
                    <ul>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>

                        <% for (int i = 1; i <= (int) request.getAttribute("PageTotal"); i++) {%>
                        <li><a href="feedback?paging=<%=i%>"><%=i%></a></li>
                            <%}%>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Body -->

        <!-- Start Footer Area -->
        <footer class="footer">
            <!-- Footer Top -->
            <div class="footer-top pt-50">
                <div class="container">
                    <div class="row">
                        <!-- SIngle -->
                        <div class="col-lg-3 col-md-6 mb-30">
                            <div class="footer-widgets-single">
                                <h2><img src="assets/img/white-logo.png"></h2>
                                <p> Lorem ipsum dolor sit amet, consectetuipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqut enim ad minim veniamquis </p>
                            </div>
                        </div>
                        <!-- SIngle -->
                        <div class="col-lg-3 col-md-6 mb-30">
                            <div class="footer-widgets-single">
                                <h3>My account</h3>
                                <ul>
                                    <li><a href="#">Sign In</a></li>
                                    <li><a href="#">View Cart</a></li>
                                    <li><a href="#">My Wishlist</a></li>
                                    <li><a href="#">Terms & Conditions</a></li>
                                    <li><a href="#">Contact us</a></li>
                                    <li><a href="#">Track My Order</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- SIngle -->
                        <div class="col-lg-3 col-md-6 mb-30">
                            <div class="footer-widgets-single">
                                <h3>Categories</h3>
                                <ul>
                                    <li><a href="#">Home Audio & Theater</a></li>
                                    <li><a href="#">TV & Video</a></li>
                                    <li><a href="#">Camera, Photo & Video</a></li>
                                    <li><a href="#">Cell Phones & Accessories</a></li>
                                    <li><a href="#">Headphones</a></li>
                                    <li><a href="#">Video Games</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- SIngle -->
                        <div class="col-lg-3 col-md-6 mb-30">
                            <div class="footer-widgets-single">
                                <h3>Newsletter</h3>
                                <p> Get notified of new products, limited releases, and more. </p>
                                <form action="#">
                                    <input type="email" name="email" placeholder="Your Email">
                                    <button type="submit" class="button-1">Subscribe</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer Bottom -->
            <div class="footer-bottom pt-30 pb-30">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="copyright-text">
                                <p> &copy; Copyright 2021 <a href="https://codepopular.com" target="_blank">CodePopular</a> All Rights Reserved. </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <ul class="footer_payment">
                                <li><a href="#"><img src="assets/img/visa.png" alt="visa"></a></li>
                                <li><a href="#"><img src="assets/img/discover.png" alt="discover"></a></li>
                                <li><a href="#"><img src="assets/img/master_card.png" alt="master_card"></a></li>
                                <li><a href="#"><img src="assets/img/paypal.png" alt="paypal"></a></li>
                                <li><a href="#"><img src="assets/img/amarican_express.png" alt="amarican_express"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer Area -->


        <!-- Js File -->
        <script src="assets/js/modernizr.min.js"></script>
        <script src="assets/js/jquery-3.5.1.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/jquery.nav.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/mixitup.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/script.js"></script>
        <script src="assets/js/mobile-menu.js"></script>
    </body>
</html>
